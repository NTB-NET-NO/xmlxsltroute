Imports System.Data
Imports System.Data.OleDb

Public Class Database
    Inherits System.ComponentModel.Component

#Region " Component Designer generated code "

    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)
    End Sub

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Component overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

#End Region

    Protected dsCustomerJob As DataSet
    Protected dsCustomer As DataSet
    Protected dsCustomerDistKode As DataSet
    Protected dsDistKodeSatAviser As DataSet

    Sub FillDataSet()
        Dim cn As OleDbConnection
        Dim dataAdapter As OleDbDataAdapter
        Dim command As OleDbCommand

        dsCustomerJob = New DataSet()
        dsCustomer = New DataSet()
        dsCustomerDistKode = New DataSet()
        dsDistKodeSatAviser = New DataSet()

        cn = New OleDbConnection()
        dataAdapter = New OleDbDataAdapter()
        command = New OleDbCommand()

        cn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "Password="""";User ID=Admin;" & _
        "Data Source=P:\Visual Studio Projects\Notabene\XmlXsltRoute\XmlXsltRoute_db.mdb;"

        dataAdapter.SelectCommand = command
        command.Connection = cn
        cn.Open()

        command.CommandText = "SELECT * FROM GetCustomer"
        dataAdapter.Fill(dsCustomer)
        dsCustomer.WriteXml("d:\temp\DataCustomer.xml")

        command.CommandText = "SELECT * FROM GetCustomerJob"
        dataAdapter.Fill(dsCustomerJob)
        dsCustomerJob.WriteXml("d:\temp\DataCustomerJob.xml")

        command.CommandText = "SELECT * FROM CustomerDistKode"
        dataAdapter.Fill(dsCustomerDistKode)
        dsCustomerDistKode.WriteXml("d:\temp\DataCustomerDistKode.xml")

        command.CommandText = "SELECT * FROM DistKodeSatAviser"
        dataAdapter.Fill(dsDistKodeSatAviser)
        dsDistKodeSatAviser.WriteXml("d:\temp\DatadsDistKodeSatAviser.xml")

        cn.Close()
        dataAdapter.Dispose()
        cn.Dispose()
        command.Dispose()

    End Sub

End Class
