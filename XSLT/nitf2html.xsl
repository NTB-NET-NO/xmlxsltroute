<<<<<<< HEAD
<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes" indent="yes"/>
<!--xsl:output method="html" encoding="ISO-8859-1"/-->

<!-- 
	Styleshet for transformasjon fra NTB internt XML-format til HTML
	Inkluderer lydfilhenvisning hvis dette finnes
	Sist endret Av Roar Vestre 27.11.2001
-->

<xsl:template match="/nitf">
  <HTML>

	<xsl:apply-templates select="head"/>
      <BODY>
	  	<h1><xsl:value-of select="body/body.head/hedline/hl1"/></h1>
		<xsl:apply-templates select="head/meta[@name='ntb-lyd']/@content"/>

<!--		
		<xsl:if test="head/meta[@name='ntb-lyd' and @content!='']">
			<a><xsl:attribute name="href">file://<xsl:value-of select="head/meta[@name='ntb-lydfil']/@content"/></xsl:attribute>Last ned lydfil</a>
		</xsl:if>

		<xsl:if test="head/meta[@name='ntb-sms' and @content!='']">
        <table border="1" cellspacing="0" cellpadding="4" >
        <tr><td>
			<b>SMS:</b>
        </td>
        <td>
        	<xsl:value-of select="head/meta[@name='ntb-sms']/@content" disable-output-escaping="yes"/>
        </td></tr>
        </table>
		</xsl:if>
-->		
		<p><xsl:value-of select="body/body.head/byline" /></p>
		<xsl:apply-templates select="body/body.content"/>

		<hr/>
		<ADDRESS>
		<xsl:apply-templates select="body/body.end"/>
		</ADDRESS>
	
		<p><i>
			Publisert:
			<xsl:value-of select="head/meta[@name='ntb-dato']/@content"/>
			</i>
		</p>

   </BODY>
  </HTML>

</xsl:template>

<xsl:template match="body/body.content">
	<xsl:apply-templates />
</xsl:template> 
  
<xsl:template match="p[@lede='true']">
  	<p class="ingress"><xsl:value-of select="."/></p>
</xsl:template> 

<xsl:template match="p">
   <xsl:copy-of select="."/>
</xsl:template> 

<xsl:template match="hl2">
  <h2><xsl:value-of select="."/></h2>
</xsl:template> 

<xsl:template match="media[@media-type='image']">
  <p><b>Bilde: <xsl:value-of select="media-reference/@source"/></b></p>
  <p><i><xsl:value-of select="media-caption"/></i></p>
</xsl:template> 

<xsl:template match="body.end/tagline">
	<p>
		<xsl:copy-of select="a"/>
	</p>
</xsl:template> 

<!-- Tabeller -->
<xsl:template match="table">
	<table class="viewTable" width="70%" border="1" cellpadding="2" cellspacing="0">
		<xsl:copy-of select="./*"/>
	</table>
</xsl:template>


<xsl:template match="head/meta[@name='ntb-lyd']/@content">
	<script language="javascript" src="http://194.19.39.29/kunde/ntb/flashsound.js"></script>
	<script language="Javascript">
		var lyd = new FlashSound();
	</script>

	<a href="javascript://" onmouseover="lyd.TGotoAndPlay('/','start')" onmouseout="lyd.TGotoAndPlay('/','stop')">
	<img src="http://194.19.39.29/kunde/ntb/grafikk/ntb.gif" border="0"/></a>
	<script>lyd.embedSWF("http://194.19.39.29/kunde/ntb/flash/<xsl:value-of select="."/>.swf");</script>

</xsl:template>

<xsl:template match="/nitf/head">
<HEAD>
<style>
<xsl:comment>
BODY
{
    FONT-WEIGHT: normal;
    LINE-HEIGHT: normal;
    FONT-STYLE: normal;
    FONT-FAMILY: Myriad, Arial, Helvetica, Geneva, Swiss, SunSans-Regular;
    TEXT-DECORATION: none
}

H1
{
    FONT-WEIGHT: bold;
    FONT-SIZE: small;
    MARGIN-BOTTOM: 10px
}


P
{
    FONT-SIZE: x-small;
    PADDING-BOTTOM: 10px;
    LINE-HEIGHT: normal;
    FONT-STYLE: normal;
    TEXT-DECORATION: none
}

.viewDate
{
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 10px
}

.ingress
{
    MARGIN-TOP: 7px;
    FONT-WEIGHT: bold;
    FONT-SIZE: x-small;
    MARGIN-BOTTOM: 5px
}

H2
{
    MARGIN-TOP: 8px;
    FONT-WEIGHT: bold;
    FONT-SIZE: x-small;
    FONT-STYLE: italic;
    FONT-VARIANT: normal
}

.viewIndent
{
    TEXT-INDENT: 20px;
    TEXT-DECORATION: none
}

.viewEdMsg
{
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 5px;
    COLOR: #d33366
}

.viewTagline
{
    MARGIN-TOP: 10px;
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 5px;
    COLOR: #003366;
    FONT-STYLE: italic
}

.viewByline
{
    FONT-SIZE: x-small;
    COLOR: #003366;
    FONT-STYLE: italic
}

</xsl:comment>
</style>
		<META>
			<xsl:attribute name="name">ntb.folder</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="meta[@name='ntb-folder']/@content"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.stoffgruppe</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="tobject/@tobject.type"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.undergruppe</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="tobject/tobject.property/@tobject.property.type"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.hovedkategori</xsl:attribute>
			<xsl:attribute name="content">
			<xsl:for-each select="//@tobject.subject.type"><xsl:value-of select="."/>;</xsl:for-each>
			</xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.omraader</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/evloc"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.fylker</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/evloc/@county-dist"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatokonomi</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='POL']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatsport</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='SPO']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatkultur</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='KUL']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatkuriosa</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='KUR']/@tobject.subject.matter"/></xsl:attribute>
		</META>


		<META>
			<xsl:attribute name="name">ntb.prioritet</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/urgency/@ed-urg"/></xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.stikkord</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/key-list/keyword/@key"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.tittel</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="/nitf/body/body.head/hedline/hl1"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.tid</xsl:attribute>
			<xsl:attribute name="content">
			<xsl:value-of select="substring(pubdata/@date.publication, 1, 8)"/>&#32;<xsl:value-of select="substring(pubdata/@date.publication, 10, 2)"/>.<xsl:value-of select="substring(pubdata/@date.publication, 12, 2)"/>.<xsl:value-of select="substring(pubdata/@date.publication, 14, 2)"/>
			</xsl:attribute>

		</META>
		<xsl:if test="meta[@name='ntb-sms']/@content!=''">
			<META>
				<xsl:attribute name="name">ntb.sms</xsl:attribute>
				<xsl:attribute name="content">ja</xsl:attribute>
			</META>
		</xsl:if>
		<xsl:if test="meta[@name='ntb-lyd']/@content!=''">
			<META>
				<xsl:attribute name="name">ntb.lyd</xsl:attribute>
				<xsl:attribute name="content">ja</xsl:attribute>
			</META>
		</xsl:if>

        <TITLE><xsl:value-of select="/nitf/body/body.head/hedline/hl1"/></TITLE>
      </HEAD>
</xsl:template>

=======
<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes" indent="yes"/>
<!--xsl:output method="html" encoding="ISO-8859-1"/-->

<!-- 
	Styleshet for transformasjon fra NTB internt XML-format til HTML
	Inkluderer lydfilhenvisning hvis dette finnes
	Sist endret Av Roar Vestre 27.11.2001
-->

<xsl:template match="/nitf">
  <HTML>

	<xsl:apply-templates select="head"/>
      <BODY>
	  	<h1><xsl:value-of select="body/body.head/hedline/hl1"/></h1>
		<xsl:apply-templates select="head/meta[@name='ntb-lyd']/@content"/>

<!--		
		<xsl:if test="head/meta[@name='ntb-lyd' and @content!='']">
			<a><xsl:attribute name="href">file://<xsl:value-of select="head/meta[@name='ntb-lydfil']/@content"/></xsl:attribute>Last ned lydfil</a>
		</xsl:if>

		<xsl:if test="head/meta[@name='ntb-sms' and @content!='']">
        <table border="1" cellspacing="0" cellpadding="4" >
        <tr><td>
			<b>SMS:</b>
        </td>
        <td>
        	<xsl:value-of select="head/meta[@name='ntb-sms']/@content" disable-output-escaping="yes"/>
        </td></tr>
        </table>
		</xsl:if>
-->		
		<p><xsl:value-of select="body/body.head/byline" /></p>
		<xsl:apply-templates select="body/body.content"/>

		<hr/>
		<ADDRESS>
		<xsl:apply-templates select="body/body.end"/>
		</ADDRESS>
	
		<p><i>
			Publisert:
			<xsl:value-of select="head/meta[@name='ntb-dato']/@content"/>
			</i>
		</p>

   </BODY>
  </HTML>

</xsl:template>

<xsl:template match="body/body.content">
	<xsl:apply-templates />
</xsl:template> 
  
<xsl:template match="p[@lede='true']">
  	<p class="ingress"><xsl:value-of select="."/></p>
</xsl:template> 

<xsl:template match="p">
   <xsl:copy-of select="."/>
</xsl:template> 

<xsl:template match="hl2">
  <h2><xsl:value-of select="."/></h2>
</xsl:template> 

<xsl:template match="media[@media-type='image']">
  <p><b>Bilde: <xsl:value-of select="media-reference/@source"/></b></p>
  <p><i><xsl:value-of select="media-caption"/></i></p>
</xsl:template> 

<xsl:template match="body.end/tagline">
	<p>
		<xsl:copy-of select="a"/>
	</p>
</xsl:template> 

<!-- Tabeller -->
<xsl:template match="table">
	<table class="viewTable" width="70%" border="1" cellpadding="2" cellspacing="0">
		<xsl:copy-of select="./*"/>
	</table>
</xsl:template>


<xsl:template match="head/meta[@name='ntb-lyd']/@content">
	<script language="javascript" src="http://194.19.39.29/kunde/ntb/flashsound.js"></script>
	<script language="Javascript">
		var lyd = new FlashSound();
	</script>

	<a href="javascript://" onmouseover="lyd.TGotoAndPlay('/','start')" onmouseout="lyd.TGotoAndPlay('/','stop')">
	<img src="http://194.19.39.29/kunde/ntb/grafikk/ntb.gif" border="0"/></a>
	<script>lyd.embedSWF("http://194.19.39.29/kunde/ntb/flash/<xsl:value-of select="."/>.swf");</script>

</xsl:template>

<xsl:template match="/nitf/head">
<HEAD>
<style>
<xsl:comment>
BODY
{
    FONT-WEIGHT: normal;
    LINE-HEIGHT: normal;
    FONT-STYLE: normal;
    FONT-FAMILY: Myriad, Arial, Helvetica, Geneva, Swiss, SunSans-Regular;
    TEXT-DECORATION: none
}

H1
{
    FONT-WEIGHT: bold;
    FONT-SIZE: small;
    MARGIN-BOTTOM: 10px
}


P
{
    FONT-SIZE: x-small;
    PADDING-BOTTOM: 10px;
    LINE-HEIGHT: normal;
    FONT-STYLE: normal;
    TEXT-DECORATION: none
}

.viewDate
{
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 10px
}

.ingress
{
    MARGIN-TOP: 7px;
    FONT-WEIGHT: bold;
    FONT-SIZE: x-small;
    MARGIN-BOTTOM: 5px
}

H2
{
    MARGIN-TOP: 8px;
    FONT-WEIGHT: bold;
    FONT-SIZE: x-small;
    FONT-STYLE: italic;
    FONT-VARIANT: normal
}

.viewIndent
{
    TEXT-INDENT: 20px;
    TEXT-DECORATION: none
}

.viewEdMsg
{
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 5px;
    COLOR: #d33366
}

.viewTagline
{
    MARGIN-TOP: 10px;
    FONT-SIZE: xx-small;
    MARGIN-BOTTOM: 5px;
    COLOR: #003366;
    FONT-STYLE: italic
}

.viewByline
{
    FONT-SIZE: x-small;
    COLOR: #003366;
    FONT-STYLE: italic
}

</xsl:comment>
</style>
		<META>
			<xsl:attribute name="name">ntb.folder</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="meta[@name='ntb-folder']/@content"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.stoffgruppe</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="tobject/@tobject.type"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.undergruppe</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="tobject/tobject.property/@tobject.property.type"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.hovedkategori</xsl:attribute>
			<xsl:attribute name="content">
			<xsl:for-each select="//@tobject.subject.type"><xsl:value-of select="."/>;</xsl:for-each>
			</xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.omraader</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/evloc"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.fylker</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/evloc/@county-dist"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatokonomi</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='POL']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatsport</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='SPO']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatkultur</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='KUL']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatkuriosa</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='KUR']/@tobject.subject.matter"/></xsl:attribute>
		</META>


		<META>
			<xsl:attribute name="name">ntb.prioritet</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/urgency/@ed-urg"/></xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.stikkord</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/key-list/keyword/@key"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.tittel</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="/nitf/body/body.head/hedline/hl1"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.tid</xsl:attribute>
			<xsl:attribute name="content">
			<xsl:value-of select="substring(pubdata/@date.publication, 1, 8)"/>&#32;<xsl:value-of select="substring(pubdata/@date.publication, 10, 2)"/>.<xsl:value-of select="substring(pubdata/@date.publication, 12, 2)"/>.<xsl:value-of select="substring(pubdata/@date.publication, 14, 2)"/>
			</xsl:attribute>

		</META>
		<xsl:if test="meta[@name='ntb-sms']/@content!=''">
			<META>
				<xsl:attribute name="name">ntb.sms</xsl:attribute>
				<xsl:attribute name="content">ja</xsl:attribute>
			</META>
		</xsl:if>
		<xsl:if test="meta[@name='ntb-lyd']/@content!=''">
			<META>
				<xsl:attribute name="name">ntb.lyd</xsl:attribute>
				<xsl:attribute name="content">ja</xsl:attribute>
			</META>
		</xsl:if>

        <TITLE><xsl:value-of select="/nitf/body/body.head/hedline/hl1"/></TITLE>
      </HEAD>
</xsl:template>

>>>>>>> ee1ac5a057562e95d3fc3b9add0412e9633c41eb
</xsl:stylesheet>