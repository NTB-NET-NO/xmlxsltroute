<<<<<<< HEAD
<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes" indent="yes"/>

<!-- 
	Styleshet for transformasjon fra NTB NITF XML-format til HTML
	Kun til bruk for WAP-l�sning med friteksts�k og kategorier 
	ASP sider med indexserver basert s�k i HTML (P� fiffus ; donald)
	Sist endret Av Roar Vestre 29.06.2005
-->

<xsl:template match="/nitf">
	<HTML>
		<xsl:apply-templates select="head"/>

		<BODY>
			<p><b><xsl:value-of select="body/body.head/hedline/hl1"/></b></p>
			<p></p>
			<xsl:apply-templates select="body/body.content"/>

			<p align="right">
				<xsl:value-of select="body/body.end/tagline/a"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="head/meta[@name='ntb-dato']/@content"/>
			</p>

		</BODY>
	</HTML>
</xsl:template>

<xsl:template match="body/body.content">
	<xsl:apply-templates />
</xsl:template> 

<xsl:template match="p[@lede='true']">
  <p><b><xsl:value-of select="."/></b></p>
</xsl:template> 

<xsl:template match="p">
  <p>
  <xsl:text>&#160;&#160;&#160;</xsl:text>
  <xsl:value-of select="."/>
  </p>
</xsl:template> 

<xsl:template match="hl2">
  <p><br/><b><xsl:value-of select="."/></b></p>
</xsl:template> 

<xsl:template match="body.end/tagline">
	<p><xsl:copy-of select="a"/></p>
</xsl:template> 

<xsl:template match="*">
</xsl:template>

<xsl:template match="/nitf/head">
	<HEAD>
		<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<META>
			<xsl:attribute name="name">ntb.folder</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="meta[@name='foldername']/@content"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.stoffgruppe</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="tobject/@tobject.type"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.undergruppe</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="tobject/tobject.property/@tobject.property.type"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.hovedkategori</xsl:attribute>
			<xsl:attribute name="content">
			<xsl:for-each select="//@tobject.subject.type"><xsl:value-of select="."/>;</xsl:for-each>
			</xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.omraader</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/evloc/@state-prov"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.fylker</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/evloc/@county-dist"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatokonomi</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='POL']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatsport</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='SPO']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatkultur</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='KUL']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatkuriosa</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='KUR']/@tobject.subject.matter"/></xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.prioritet</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/urgency/@ed-urg"/></xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.stikkord</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/key-list/keyword/@key"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.tittel</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="/nitf/body/body.head/hedline/hl1"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.tid</xsl:attribute>
			<xsl:attribute name="content">
			<xsl:value-of select="substring(pubdata/@date.publication, 1, 8)"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="substring(pubdata/@date.publication, 10, 2)"/>
			<xsl:text>.</xsl:text>
			<xsl:value-of select="substring(pubdata/@date.publication, 12, 2)"/>
			<xsl:text>.</xsl:text>
			<xsl:value-of select="substring(pubdata/@date.publication, 14, 2)"/>
			</xsl:attribute>
			
		</META>
		<xsl:if test="meta[@name='ntb-sms']/@content!=''">
			<META>
				<xsl:attribute name="name">ntb.sms</xsl:attribute>
				<xsl:attribute name="content">ja</xsl:attribute>
			</META>
		</xsl:if>
		<xsl:if test="meta[@name='ntb-lyd']/@content!=''">
			<META>
				<xsl:attribute name="name">ntb.lyd</xsl:attribute>
				<xsl:attribute name="content">ja</xsl:attribute>
			</META>
		</xsl:if>

        <TITLE><xsl:value-of select="/nitf/body/body.head/hedline/hl1"/></TITLE>
      </HEAD>
</xsl:template>

=======
<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="yes" standalone="yes" indent="yes"/>

<!-- 
	Styleshet for transformasjon fra NTB NITF XML-format til HTML
	Kun til bruk for WAP-l�sning med friteksts�k og kategorier 
	ASP sider med indexserver basert s�k i HTML (P� fiffus ; donald)
	Sist endret Av Roar Vestre 29.06.2005
-->

<xsl:template match="/nitf">
	<HTML>
		<xsl:apply-templates select="head"/>

		<BODY>
			<p><b><xsl:value-of select="body/body.head/hedline/hl1"/></b></p>
			<p></p>
			<xsl:apply-templates select="body/body.content"/>

			<p align="right">
				<xsl:value-of select="body/body.end/tagline/a"/>
				<xsl:text> </xsl:text>
				<xsl:value-of select="head/meta[@name='ntb-dato']/@content"/>
			</p>

		</BODY>
	</HTML>
</xsl:template>

<xsl:template match="body/body.content">
	<xsl:apply-templates />
</xsl:template> 

<xsl:template match="p[@lede='true']">
  <p><b><xsl:value-of select="."/></b></p>
</xsl:template> 

<xsl:template match="p">
  <p>
  <xsl:text>&#160;&#160;&#160;</xsl:text>
  <xsl:value-of select="."/>
  </p>
</xsl:template> 

<xsl:template match="hl2">
  <p><br/><b><xsl:value-of select="."/></b></p>
</xsl:template> 

<xsl:template match="body.end/tagline">
	<p><xsl:copy-of select="a"/></p>
</xsl:template> 

<xsl:template match="*">
</xsl:template>

<xsl:template match="/nitf/head">
	<HEAD>
		<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
		<META>
			<xsl:attribute name="name">ntb.folder</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="meta[@name='foldername']/@content"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.stoffgruppe</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="tobject/@tobject.type"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.undergruppe</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="tobject/tobject.property/@tobject.property.type"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.hovedkategori</xsl:attribute>
			<xsl:attribute name="content">
			<xsl:for-each select="//@tobject.subject.type"><xsl:value-of select="."/>;</xsl:for-each>
			</xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.omraader</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/evloc/@state-prov"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.fylker</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/evloc/@county-dist"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatokonomi</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='POL']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatsport</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='SPO']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatkultur</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='KUL']/@tobject.subject.matter"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.underkatkuriosa</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="//tobject.subject[@tobject.subject.code='KUR']/@tobject.subject.matter"/></xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.prioritet</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/urgency/@ed-urg"/></xsl:attribute>
		</META>

		<META>
			<xsl:attribute name="name">ntb.stikkord</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="docdata/key-list/keyword/@key"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.tittel</xsl:attribute>
			<xsl:attribute name="content"><xsl:value-of select="/nitf/body/body.head/hedline/hl1"/></xsl:attribute>
		</META>
		<META>
			<xsl:attribute name="name">ntb.tid</xsl:attribute>
			<xsl:attribute name="content">
			<xsl:value-of select="substring(pubdata/@date.publication, 1, 8)"/>
			<xsl:text> </xsl:text>
			<xsl:value-of select="substring(pubdata/@date.publication, 10, 2)"/>
			<xsl:text>.</xsl:text>
			<xsl:value-of select="substring(pubdata/@date.publication, 12, 2)"/>
			<xsl:text>.</xsl:text>
			<xsl:value-of select="substring(pubdata/@date.publication, 14, 2)"/>
			</xsl:attribute>
			
		</META>
		<xsl:if test="meta[@name='ntb-sms']/@content!=''">
			<META>
				<xsl:attribute name="name">ntb.sms</xsl:attribute>
				<xsl:attribute name="content">ja</xsl:attribute>
			</META>
		</xsl:if>
		<xsl:if test="meta[@name='ntb-lyd']/@content!=''">
			<META>
				<xsl:attribute name="name">ntb.lyd</xsl:attribute>
				<xsl:attribute name="content">ja</xsl:attribute>
			</META>
		</xsl:if>

        <TITLE><xsl:value-of select="/nitf/body/body.head/hedline/hl1"/></TITLE>
      </HEAD>
</xsl:template>

>>>>>>> ee1ac5a057562e95d3fc3b9add0412e9633c41eb
</xsl:stylesheet>