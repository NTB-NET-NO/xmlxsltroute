<<<<<<< HEAD
<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" />
	<xsl:param name="lastBuildDate"></xsl:param>
	<xsl:param name="relativeOutputPath"></xsl:param>
	<xsl:template match="/nitf">
		<rss version="2.0">
			<channel>
				<title>
					<xsl:text>NTB RSS Full Feed</xsl:text>
				</title>
				<link>
					<xsl:text>http://www.ntb.no/RSS/</xsl:text>
					<xsl:value-of select="$relativeOutputPath" />
				</link>
				<description>NTB RSS Full Feed</description>
				<pubDate>
					<xsl:value-of select="head/pubdata/@date.publication" />
				</pubDate>
				<lastBuildDate>
					<xsl:value-of select="$lastBuildDate" />
				</lastBuildDate>
				<copyright>
					<xsl:value-of select="head/docdata/doc.copyright/@holder" />
				</copyright>
				<docs>http://www.rssboard.org/rss-specification</docs>
				<item>
					<title>
						<xsl:value-of select="head/title | head/docdata | head/pubdata | head/revision-history  | head/tobject" />
					</title>
					<description>
						<xsl:call-template name="replace-string">
							<xsl:with-param name="text" select="body/body.content" />
							<xsl:with-param name="from">hl2</xsl:with-param>
							<xsl:with-param name="to">b</xsl:with-param>
						</xsl:call-template>
					</description>
					<pubDate>
						<xsl:value-of select="head/pubdata/@date.publication" />
					</pubDate>
					<guid>
						<xsl:value-of select="head/docdata/du-key/@key" />
					</guid>
				</item>
			</channel>
		</rss>
	</xsl:template>
	<xsl:template name="replace-string">
		<xsl:param name="text" />
		<xsl:param name="from" />
		<xsl:param name="to" />
		<xsl:choose>
			<xsl:when test="contains($text, $from)">
				<xsl:variable name="before" select="substring-before($text, $from)" />
				<xsl:variable name="after" select="substring-after($text, $from)" />
				<xsl:variable name="prefix" select="concat($before, $to)" />
				<xsl:copy-of select="$before" />
				<xsl:copy-of select="$to" />
				<xsl:call-template name="replace-string">
					<xsl:with-param name="text" select="$after" />
					<xsl:with-param name="from" select="$from" />
					<xsl:with-param name="to" select="$to" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
=======
<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="UTF-8" indent="yes" />
	<xsl:param name="lastBuildDate"></xsl:param>
	<xsl:param name="relativeOutputPath"></xsl:param>
	<xsl:template match="/nitf">
		<rss version="2.0">
			<channel>
				<title>
					<xsl:text>NTB RSS Full Feed</xsl:text>
				</title>
				<link>
					<xsl:text>http://www.ntb.no/RSS/</xsl:text>
					<xsl:value-of select="$relativeOutputPath" />
				</link>
				<description>NTB RSS Full Feed</description>
				<pubDate>
					<xsl:value-of select="head/pubdata/@date.publication" />
				</pubDate>
				<lastBuildDate>
					<xsl:value-of select="$lastBuildDate" />
				</lastBuildDate>
				<copyright>
					<xsl:value-of select="head/docdata/doc.copyright/@holder" />
				</copyright>
				<docs>http://www.rssboard.org/rss-specification</docs>
				<item>
					<title>
						<xsl:value-of select="head/title | head/docdata | head/pubdata | head/revision-history  | head/tobject" />
					</title>
					<description>
						<xsl:call-template name="replace-string">
							<xsl:with-param name="text" select="body/body.content" />
							<xsl:with-param name="from">hl2</xsl:with-param>
							<xsl:with-param name="to">b</xsl:with-param>
						</xsl:call-template>
					</description>
					<pubDate>
						<xsl:value-of select="head/pubdata/@date.publication" />
					</pubDate>
					<guid>
						<xsl:value-of select="head/docdata/du-key/@key" />
					</guid>
				</item>
			</channel>
		</rss>
	</xsl:template>
	<xsl:template name="replace-string">
		<xsl:param name="text" />
		<xsl:param name="from" />
		<xsl:param name="to" />
		<xsl:choose>
			<xsl:when test="contains($text, $from)">
				<xsl:variable name="before" select="substring-before($text, $from)" />
				<xsl:variable name="after" select="substring-after($text, $from)" />
				<xsl:variable name="prefix" select="concat($before, $to)" />
				<xsl:copy-of select="$before" />
				<xsl:copy-of select="$to" />
				<xsl:call-template name="replace-string">
					<xsl:with-param name="text" select="$after" />
					<xsl:with-param name="from" select="$from" />
					<xsl:with-param name="to" select="$to" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
>>>>>>> ee1ac5a057562e95d3fc3b9add0412e9633c41eb
