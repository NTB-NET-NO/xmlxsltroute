<<<<<<< HEAD
<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
	XSLT Stylesheet by NTB Roar Vestre
	Must run XchgReplace for string conversions aftewards:
	1) Strip all CrLf
	2) Make tag-{crlf} to CrLf
	3) Convert HEX (&#x02;) or Decimal (&#148;) entities to corresponding Ascii-values (codepage 1252)
	
	Last Change 01.12.2002 by RoV, Converted to IPTC. 
	Last Change 24.04.2003 by RoV, Added tables
	Last Change 24.04.2003 by RoV, Added Choose for Meldingstype i 
		<xsl:template name="gruppe"> og <xsl:template name="undergruppe">
	Last Change v5 23.05.2003 by RoV
		
-->

<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
xmlns:user="http://ntb.no/mynamespace">

<xsl:output method="text"/>
<xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>
<xsl:strip-space elements="*"/>

<xsl:variable name="crlf">
<xsl:text>{crlf}
</xsl:text>
</xsl:variable>

<!--
For � skifte mellom XSLT i MsXml 4 og i .NET, m� language attributtet for <msxsl:script> og 
konstanten DOTNET i VB koden nedenfor settes til et av f�lgende verdipar:
1) .NET:	<msxsl:script language="vb" implements-prefix="user">		: DOTNET = true
2) MsXml4:	<msxsl:script language="VBScript" implements-prefix="user">	: DOTNET = false
-->
<msxsl:script language="VBScript" implements-prefix="user">
<![CDATA[
	Const DOTNET = False
 
	Function MakeLineBreaks(nodeListInput)
		Dim intPos, intPos2, intCRlen
		Const LINE_LEN = 65
		Const CR = "{crlf}"
		dim strText

		if DOTNET then
			'strText = nodeListInput.Current.Value
			strText = nodeListInput
		else
			strText = nodeListInput.nextNode().Text
		end if	
		
		intCRlen = Len(CR)
		intPos = LINE_LEN
		Do
			intPos = InStrRev(strText, " ", intPos)
			If intPos = 0 Or intPos = intPos2 Then 
        		Exit Do
			End If
			strText = Mid(strText, 1, intPos) + CR + Mid(strText, intPos + 1)
			intPos2 = intPos
			intPos = intPos + LINE_LEN + intCRlen
		Loop

		MakeLineBreaks = strText
	End Function

	Function IptcDate(nodeListInput)
		dim strDate, strDate1, strMonth, strYear
		
		if DOTNET then
			'strDate = nodeListInput.Current.Value
			strDate = nodeListInput
		else	
			strDate = nodeListInput.nextNode().Text
		end if		

		' 20020923T133136
		' 211738 SEP 02
		strMonth = mid(strDate, 5 ,2)
		Select Case strMonth
			case "01"
				strMonth = "JAN"
			case "02"
				strMonth = "FEB"
			case "03"
				strMonth = "MAR"
			case "04"
				strMonth = "APR"
			case "05"
				strMonth = "MAY"
			case "06"
				strMonth = "JUN"
			case "07"
				strMonth = "JUL"
			case "08"
				strMonth = "AUG"
			case "09"
				strMonth = "SEP"
			case "10"
				strMonth = "OCT"
			case "11"
				strMonth = "NOV"
			case "12"
				strMonth = "DEC"
			case else
				strMont = "MMM"
		End Select

		strDate1 = mid(strDate, 7 ,2) + mid(strDate, 10 ,4) + " " + strMonth + " " + mid(strDate, 3 ,2)
		IptcDate = strDate1
	End Function
]]>
</msxsl:script>

<xsl:template match="/nitf">
	<xsl:text>&amp;#x01;NT</xsl:text><xsl:value-of select="head/meta[@name='NTBKanal']/@content"/>
	<xsl:value-of select="head/meta[@name='NTBIPTCSequence']/@content"/>
	<xsl:text>&#32;</xsl:text>
	
	<xsl:value-of select="head/docdata/urgency/@ed-urg"/>
	<xsl:text>&#32;</xsl:text>
	<xsl:call-template name="gruppe"/>
	<xsl:text>&#32;</xsl:text>
	
	<xsl:value-of select="format-number(string-length(body/body.content), '00000')"/> 
	<xsl:text>&#32;</xsl:text>
	<xsl:call-template name="undergruppe"/>
	<xsl:text>&#32;</xsl:text>
	<xsl:value-of select="head/revision-history/@name"/>
	<xsl:value-of select="$crlf"/>

	<xsl:value-of select="head/meta[@name='subject']/@content"/>
	<!--<xsl:text>&#10;</xsl:text>-->
	<xsl:value-of select="$crlf"/>
	<xsl:value-of select="$crlf"/>

	<xsl:text>&amp;#x02;&amp;#x98;</xsl:text>
	<xsl:value-of select="body/body.head/hedline/hl1"/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:if test="head/docdata/ed-msg[@info!='']">
		<xsl:text>&amp;#x99;(Til red: </xsl:text>
		<xsl:value-of select="user:MakeLineBreaks(head/docdata/ed-msg/@info)" />
		<xsl:text>)&amp;#x80;</xsl:text>
		<xsl:value-of select="$crlf"/>
	</xsl:if>

	<!--
	<xsl:text>&amp;#x92;</xsl:text>
	<xsl:value-of select="body/body.content/p[@lede='true']"/>
	<xsl:text>&amp;#xB6;</xsl:text>
	-->

	<xsl:apply-templates select="body/body.head/byline"/>
	<xsl:apply-templates select="body/body.content"/>

	<xsl:value-of select="$crlf"/>

	<xsl:if test="body/body.content/media[@media-type='image']">
		<xsl:text>&amp;#x80;</xsl:text>
		<xsl:text>-Bildetekst(er):</xsl:text>
		<xsl:apply-templates select="body/body.content/media[@media-type='image']"/>
		<xsl:text>&amp;#x80;</xsl:text>
	</xsl:if>

	<xsl:apply-templates select="body/body.end/tagline"/>

	<xsl:text>&amp;#x03;</xsl:text>
	<xsl:value-of select="user:IptcDate(head/pubdata/@date.publication)"/>
	<xsl:text>&amp;#x0D;&amp;#x0D;&amp;#x0D;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text>&amp;#x04;</xsl:text>
	<xsl:value-of select="$crlf"/>

</xsl:template>

<!-- Templates: -->
<xsl:template name="gruppe">
	<xsl:variable name="type">
		<xsl:value-of select="head/tobject/@tobject.type"/>
	</xsl:variable>

	<xsl:variable name="utype">
		<xsl:value-of select="head/tobject/tobject.property/@tobject.property.type"/>
	</xsl:variable>
	
	<xsl:choose>

		<!-- Spesialtilfelle for Kultur Nyheter -->
		<xsl:when test="head/tobject/tobject.subject[@tobject.subject.code='KUL']">
			<xsl:text>ART</xsl:text>
		</xsl:when>

		<xsl:when test="$type='Innenriks'">
			<xsl:text>INN</xsl:text>
		</xsl:when>
		
		<xsl:when test="$type='Utenriks'">
			<xsl:text>UTE</xsl:text>
		</xsl:when>

		<xsl:when test="$type='Sport' or $type='Langodds' or $type='Tippefakta' or $type='Tippevurdering' or $type='V75' or $type='V5/DD'">
			<xsl:text>SPO</xsl:text>
		</xsl:when>

		<xsl:when test="$type='PRM-NTB' or $type='PRM-BWI' or $type='PRM-NWA'">
			<xsl:text>PRM</xsl:text>
		</xsl:when>

		<xsl:when test="$type='Priv-til-red' or $utype='Menyer, til red.'">
			<xsl:text>PRI</xsl:text>
		</xsl:when>

<!--		
		<xsl:when test="$type='Kultur og underholdning'">
			<xsl:text>ART</xsl:text>
		</xsl:when>
-->
		<xsl:otherwise>
			<xsl:text>ART</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="undergruppe">
	<xsl:variable name="type">
		<xsl:value-of select="head/tobject/@tobject.type"/>
	</xsl:variable>

	<xsl:variable name="utype">
		<xsl:value-of select="head/tobject/tobject.property/@tobject.property.type"/>
	</xsl:variable>
	
	<xsl:choose>

		<!-- Spesialtilfelle for Kultur Nyheter -->
		<xsl:when test="head/tobject/tobject.subject[@tobject.subject.code='KUL']">
			<xsl:text>KUL</xsl:text>
		</xsl:when>

		<xsl:when test="$utype='Tabeller og resultater' and body/body.content/table">
			<xsl:text>TAB</xsl:text>
		</xsl:when>

		<xsl:when test="$utype='Tabeller og resultater'">
			<xsl:text>RES</xsl:text>
		</xsl:when>

		<xsl:when test="$utype='Analyse' or $type='Faktaboks' or $type='Bakgrunn'">
			<xsl:text>BAK</xsl:text>
		</xsl:when>

		<xsl:when test="$type='PRM-BWI'">
			<xsl:text>BWI</xsl:text>
		</xsl:when>

		<xsl:when test="$type='Kultur og underholdning'">
			<xsl:text>KUL</xsl:text>
		</xsl:when>
<!--		

		Feature
		PRM-tjenesten
		Oversikter
		Menyer, til red.
		Fulltekstmeldinger
		Notiser
		Faktaboks
		
-->
		
		<xsl:otherwise>
			<xsl:text>NNN</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="body/body.content">
	<xsl:apply-templates select="p | hl2 | table | br"/>
	
	<!--
	<xsl:choose>
		<xsl:when test="p">
			<xsl:apply-templates select="p | hl2 | table | br"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>&amp;#x9E;&amp;#x94;</xsl:text>
			<xsl:value-of select="user:MakeLineBreaks(.)"/>
			<xsl:text>&amp;#xB6;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	-->

</xsl:template>

<xsl:template match="body/body.head/byline">
	<xsl:text>&amp;#x93;</xsl:text>
	<xsl:value-of select="user:MakeLineBreaks(.)"/>
	<xsl:text>&amp;#xB6;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Normal paragraphs -->
<xsl:template match="p[.!='']">
	<xsl:text>&amp;#x9E;&amp;#x94;</xsl:text>
	<xsl:value-of select="user:MakeLineBreaks(.)"/>
	<xsl:text>&amp;#xB6;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Empty paragraphs -->
<xsl:template match="p[.='']"/>

<!-- Paragraph of "ingress" -->
<xsl:template match="p[@lede='true' and . !='']">
<!--<xsl:template match="p[position() = 1 and . !='']">-->
	<xsl:text>&amp;#x92;</xsl:text>
	<xsl:value-of select="user:MakeLineBreaks(.)"/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Paragraph of "Br�dtekst innrykk" -->
<!--
<xsl:template match="p[@innrykk='true']">
	<xsl:text>&amp;#158;&amp;#148;</xsl:text>
	<xsl:value-of select="."/>
	<xsl:text>&amp;#182;</xsl:text>
</xsl:template>
-->

<!-- Paragraph with no extra IPTC codes" -->
<xsl:template match="p[@class='notext']">
	<!-- <xsl:text>&amp;#158;&amp;#148;</xsl:text> -->
	<xsl:value-of select="."/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Added for handeling some messages from Fretex with <br> as paragraph tags -->
<xsl:template match="br">
		<xsl:value-of select="."/>
</xsl:template>

<!-- Mellomtittel -->
<xsl:template match="hl2">
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#x90;</xsl:text>
	<xsl:value-of select="."/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Mellomtittel, no extra codes -->
<xsl:template match="hl2[@class='notext']">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Paragraph of "tabellkode" -->
<xsl:template match="p[@class='table-code']">
	<xsl:text>&amp;#148;&amp;#148;&amp;#154;</xsl:text>
	<xsl:value-of select="substring-after(substring-before(., ']'), '[')"/>
	<xsl:text>&amp;#159;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Andre Tabeller -->
<xsl:template match="table">
	<xsl:apply-templates/>
	<xsl:text>&amp;#149;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text>&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
<!--
	<xsl:text>&amp;#148;&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
-->	
</xsl:template>

<xsl:template match="tr">
	<xsl:text>&#160;</xsl:text>
	<xsl:apply-templates/>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:template match="td[position()='1']">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#136;&amp;#132;</xsl:text>
</xsl:template>

<xsl:template match="td[@align='right']">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#134;</xsl:text>
</xsl:template>

<xsl:template match="td[@align='center']">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#133;</xsl:text>
</xsl:template>

<xsl:template match="td">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#132;</xsl:text>
</xsl:template>

<!-- Tabell t01u2 -->
<xsl:template match="table[@class='t01u2']">
	<xsl:apply-templates mode="t01u2"/>
</xsl:template>

<xsl:template match="tr" mode="t01u2">
	<xsl:text>&#160;</xsl:text>
	<xsl:apply-templates mode="t01u2"/>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:template match="td[position()='1']" mode="t01u2">
	<xsl:value-of select="substring-before(., '-')"/>
	<xsl:text>&amp;#208;</xsl:text>
	<xsl:value-of select="substring-after(., '-')"/>
	<xsl:text>&amp;#135;&amp;#132;&#32;</xsl:text>		
</xsl:template>

<xsl:template match="td[position()>'1']" mode="t01u2">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#134;</xsl:text>
</xsl:template>

<xsl:template match="td[.='-']" mode="t01u2">
	<xsl:text>&amp;#208;&amp;#133;&#32;</xsl:text>
</xsl:template>

<!-- Tabell t01u5 -->
<xsl:template match="table[@class='t01u5']">
	<xsl:apply-templates mode="t01u5"/>

	<xsl:text>&amp;#149;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#148;&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:template match="tr" mode="t01u5">
	<xsl:text>&#160;</xsl:text>
	<xsl:apply-templates mode="t01u5"/>

	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:template match="td[position()='1']" mode="t01u5">
	<xsl:value-of select="."/>
	<xsl:value-of select="substring('              ', string-length(.))"/>

	<xsl:text>&amp;#136;&amp;#132;</xsl:text>
</xsl:template>

<xsl:template match="td[position()>'1']" mode="t01u5">
	<xsl:value-of select="substring('   ', string-length(.))"/>
	<xsl:value-of select="."/>

	<xsl:text>&amp;#134;</xsl:text>
</xsl:template>

<xsl:template match="td[.='-']" mode="t01u5">
	<xsl:text>&amp;#208;&amp;#133;</xsl:text>
</xsl:template>

<!-- Article author e-mail (in NTB used as signature) -->
<xsl:template match="tagline">
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#x99;</xsl:text>

	<xsl:value-of select="a"/>

	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Pictures with Caption -->
<xsl:template match="body/body.content/media[@media-type='image']">
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>Scanpix ID: </xsl:text>

	<xsl:value-of select="media-reference/@source"/>

	<xsl:value-of select="$crlf"/>

	<xsl:value-of select="user:MakeLineBreaks(media-caption)"/>
	<!--
	<xsl:choose>
		<xsl:when test="H�yde">
			<xsl:text> (H�ydebilde)</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text> (Breddebilde)</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	-->

	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#x80;</xsl:text>
</xsl:template>

=======
<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
	XSLT Stylesheet by NTB Roar Vestre
	Must run XchgReplace for string conversions aftewards:
	1) Strip all CrLf
	2) Make tag-{crlf} to CrLf
	3) Convert HEX (&#x02;) or Decimal (&#148;) entities to corresponding Ascii-values (codepage 1252)
	
	Last Change 01.12.2002 by RoV, Converted to IPTC. 
	Last Change 24.04.2003 by RoV, Added tables
	Last Change 24.04.2003 by RoV, Added Choose for Meldingstype i 
		<xsl:template name="gruppe"> og <xsl:template name="undergruppe">
	Last Change v5 23.05.2003 by RoV
		
-->

<xsl:stylesheet version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
xmlns:user="http://ntb.no/mynamespace">

<xsl:output method="text"/>
<xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>
<xsl:strip-space elements="*"/>

<xsl:variable name="crlf">
<xsl:text>{crlf}
</xsl:text>
</xsl:variable>

<!--
For � skifte mellom XSLT i MsXml 4 og i .NET, m� language attributtet for <msxsl:script> og 
konstanten DOTNET i VB koden nedenfor settes til et av f�lgende verdipar:
1) .NET:	<msxsl:script language="vb" implements-prefix="user">		: DOTNET = true
2) MsXml4:	<msxsl:script language="VBScript" implements-prefix="user">	: DOTNET = false
-->
<msxsl:script language="VBScript" implements-prefix="user">
<![CDATA[
	Const DOTNET = False
 
	Function MakeLineBreaks(nodeListInput)
		Dim intPos, intPos2, intCRlen
		Const LINE_LEN = 65
		Const CR = "{crlf}"
		dim strText

		if DOTNET then
			'strText = nodeListInput.Current.Value
			strText = nodeListInput
		else
			strText = nodeListInput.nextNode().Text
		end if	
		
		intCRlen = Len(CR)
		intPos = LINE_LEN
		Do
			intPos = InStrRev(strText, " ", intPos)
			If intPos = 0 Or intPos = intPos2 Then 
        		Exit Do
			End If
			strText = Mid(strText, 1, intPos) + CR + Mid(strText, intPos + 1)
			intPos2 = intPos
			intPos = intPos + LINE_LEN + intCRlen
		Loop

		MakeLineBreaks = strText
	End Function

	Function IptcDate(nodeListInput)
		dim strDate, strDate1, strMonth, strYear
		
		if DOTNET then
			'strDate = nodeListInput.Current.Value
			strDate = nodeListInput
		else	
			strDate = nodeListInput.nextNode().Text
		end if		

		' 20020923T133136
		' 211738 SEP 02
		strMonth = mid(strDate, 5 ,2)
		Select Case strMonth
			case "01"
				strMonth = "JAN"
			case "02"
				strMonth = "FEB"
			case "03"
				strMonth = "MAR"
			case "04"
				strMonth = "APR"
			case "05"
				strMonth = "MAY"
			case "06"
				strMonth = "JUN"
			case "07"
				strMonth = "JUL"
			case "08"
				strMonth = "AUG"
			case "09"
				strMonth = "SEP"
			case "10"
				strMonth = "OCT"
			case "11"
				strMonth = "NOV"
			case "12"
				strMonth = "DEC"
			case else
				strMont = "MMM"
		End Select

		strDate1 = mid(strDate, 7 ,2) + mid(strDate, 10 ,4) + " " + strMonth + " " + mid(strDate, 3 ,2)
		IptcDate = strDate1
	End Function
]]>
</msxsl:script>

<xsl:template match="/nitf">
	<xsl:text>&amp;#x01;NT</xsl:text><xsl:value-of select="head/meta[@name='NTBKanal']/@content"/>
	<xsl:value-of select="head/meta[@name='NTBIPTCSequence']/@content"/>
	<xsl:text>&#32;</xsl:text>
	
	<xsl:value-of select="head/docdata/urgency/@ed-urg"/>
	<xsl:text>&#32;</xsl:text>
	<xsl:call-template name="gruppe"/>
	<xsl:text>&#32;</xsl:text>
	
	<xsl:value-of select="format-number(string-length(body/body.content), '00000')"/> 
	<xsl:text>&#32;</xsl:text>
	<xsl:call-template name="undergruppe"/>
	<xsl:text>&#32;</xsl:text>
	<xsl:value-of select="head/revision-history/@name"/>
	<xsl:value-of select="$crlf"/>

	<xsl:value-of select="head/meta[@name='subject']/@content"/>
	<!--<xsl:text>&#10;</xsl:text>-->
	<xsl:value-of select="$crlf"/>
	<xsl:value-of select="$crlf"/>

	<xsl:text>&amp;#x02;&amp;#x98;</xsl:text>
	<xsl:value-of select="body/body.head/hedline/hl1"/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:if test="head/docdata/ed-msg[@info!='']">
		<xsl:text>&amp;#x99;(Til red: </xsl:text>
		<xsl:value-of select="user:MakeLineBreaks(head/docdata/ed-msg/@info)" />
		<xsl:text>)&amp;#x80;</xsl:text>
		<xsl:value-of select="$crlf"/>
	</xsl:if>

	<!--
	<xsl:text>&amp;#x92;</xsl:text>
	<xsl:value-of select="body/body.content/p[@lede='true']"/>
	<xsl:text>&amp;#xB6;</xsl:text>
	-->

	<xsl:apply-templates select="body/body.head/byline"/>
	<xsl:apply-templates select="body/body.content"/>

	<xsl:value-of select="$crlf"/>

	<xsl:if test="body/body.content/media[@media-type='image']">
		<xsl:text>&amp;#x80;</xsl:text>
		<xsl:text>-Bildetekst(er):</xsl:text>
		<xsl:apply-templates select="body/body.content/media[@media-type='image']"/>
		<xsl:text>&amp;#x80;</xsl:text>
	</xsl:if>

	<xsl:apply-templates select="body/body.end/tagline"/>

	<xsl:text>&amp;#x03;</xsl:text>
	<xsl:value-of select="user:IptcDate(head/pubdata/@date.publication)"/>
	<xsl:text>&amp;#x0D;&amp;#x0D;&amp;#x0D;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text>&amp;#x04;</xsl:text>
	<xsl:value-of select="$crlf"/>

</xsl:template>

<!-- Templates: -->
<xsl:template name="gruppe">
	<xsl:variable name="type">
		<xsl:value-of select="head/tobject/@tobject.type"/>
	</xsl:variable>

	<xsl:variable name="utype">
		<xsl:value-of select="head/tobject/tobject.property/@tobject.property.type"/>
	</xsl:variable>
	
	<xsl:choose>

		<!-- Spesialtilfelle for Kultur Nyheter -->
		<xsl:when test="head/tobject/tobject.subject[@tobject.subject.code='KUL']">
			<xsl:text>ART</xsl:text>
		</xsl:when>

		<xsl:when test="$type='Innenriks'">
			<xsl:text>INN</xsl:text>
		</xsl:when>
		
		<xsl:when test="$type='Utenriks'">
			<xsl:text>UTE</xsl:text>
		</xsl:when>

		<xsl:when test="$type='Sport' or $type='Langodds' or $type='Tippefakta' or $type='Tippevurdering' or $type='V75' or $type='V5/DD'">
			<xsl:text>SPO</xsl:text>
		</xsl:when>

		<xsl:when test="$type='PRM-NTB' or $type='PRM-BWI' or $type='PRM-NWA'">
			<xsl:text>PRM</xsl:text>
		</xsl:when>

		<xsl:when test="$type='Priv-til-red' or $utype='Menyer, til red.'">
			<xsl:text>PRI</xsl:text>
		</xsl:when>

<!--		
		<xsl:when test="$type='Kultur og underholdning'">
			<xsl:text>ART</xsl:text>
		</xsl:when>
-->
		<xsl:otherwise>
			<xsl:text>ART</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="undergruppe">
	<xsl:variable name="type">
		<xsl:value-of select="head/tobject/@tobject.type"/>
	</xsl:variable>

	<xsl:variable name="utype">
		<xsl:value-of select="head/tobject/tobject.property/@tobject.property.type"/>
	</xsl:variable>
	
	<xsl:choose>

		<!-- Spesialtilfelle for Kultur Nyheter -->
		<xsl:when test="head/tobject/tobject.subject[@tobject.subject.code='KUL']">
			<xsl:text>KUL</xsl:text>
		</xsl:when>

		<xsl:when test="$utype='Tabeller og resultater' and body/body.content/table">
			<xsl:text>TAB</xsl:text>
		</xsl:when>

		<xsl:when test="$utype='Tabeller og resultater'">
			<xsl:text>RES</xsl:text>
		</xsl:when>

		<xsl:when test="$utype='Analyse' or $type='Faktaboks' or $type='Bakgrunn'">
			<xsl:text>BAK</xsl:text>
		</xsl:when>

		<xsl:when test="$type='PRM-BWI'">
			<xsl:text>BWI</xsl:text>
		</xsl:when>

		<xsl:when test="$type='Kultur og underholdning'">
			<xsl:text>KUL</xsl:text>
		</xsl:when>
<!--		

		Feature
		PRM-tjenesten
		Oversikter
		Menyer, til red.
		Fulltekstmeldinger
		Notiser
		Faktaboks
		
-->
		
		<xsl:otherwise>
			<xsl:text>NNN</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="body/body.content">
	<xsl:apply-templates select="p | hl2 | table | br"/>
	
	<!--
	<xsl:choose>
		<xsl:when test="p">
			<xsl:apply-templates select="p | hl2 | table | br"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>&amp;#x9E;&amp;#x94;</xsl:text>
			<xsl:value-of select="user:MakeLineBreaks(.)"/>
			<xsl:text>&amp;#xB6;</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	-->

</xsl:template>

<xsl:template match="body/body.head/byline">
	<xsl:text>&amp;#x93;</xsl:text>
	<xsl:value-of select="user:MakeLineBreaks(.)"/>
	<xsl:text>&amp;#xB6;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Normal paragraphs -->
<xsl:template match="p[.!='']">
	<xsl:text>&amp;#x9E;&amp;#x94;</xsl:text>
	<xsl:value-of select="user:MakeLineBreaks(.)"/>
	<xsl:text>&amp;#xB6;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Empty paragraphs -->
<xsl:template match="p[.='']"/>

<!-- Paragraph of "ingress" -->
<xsl:template match="p[@lede='true' and . !='']">
<!--<xsl:template match="p[position() = 1 and . !='']">-->
	<xsl:text>&amp;#x92;</xsl:text>
	<xsl:value-of select="user:MakeLineBreaks(.)"/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Paragraph of "Br�dtekst innrykk" -->
<!--
<xsl:template match="p[@innrykk='true']">
	<xsl:text>&amp;#158;&amp;#148;</xsl:text>
	<xsl:value-of select="."/>
	<xsl:text>&amp;#182;</xsl:text>
</xsl:template>
-->

<!-- Paragraph with no extra IPTC codes" -->
<xsl:template match="p[@class='notext']">
	<!-- <xsl:text>&amp;#158;&amp;#148;</xsl:text> -->
	<xsl:value-of select="."/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Added for handeling some messages from Fretex with <br> as paragraph tags -->
<xsl:template match="br">
		<xsl:value-of select="."/>
</xsl:template>

<!-- Mellomtittel -->
<xsl:template match="hl2">
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#x90;</xsl:text>
	<xsl:value-of select="."/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Mellomtittel, no extra codes -->
<xsl:template match="hl2[@class='notext']">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Paragraph of "tabellkode" -->
<xsl:template match="p[@class='table-code']">
	<xsl:text>&amp;#148;&amp;#148;&amp;#154;</xsl:text>
	<xsl:value-of select="substring-after(substring-before(., ']'), '[')"/>
	<xsl:text>&amp;#159;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Andre Tabeller -->
<xsl:template match="table">
	<xsl:apply-templates/>
	<xsl:text>&amp;#149;</xsl:text>
	<xsl:value-of select="$crlf"/>

	<xsl:text>&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
<!--
	<xsl:text>&amp;#148;&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
-->	
</xsl:template>

<xsl:template match="tr">
	<xsl:text>&#160;</xsl:text>
	<xsl:apply-templates/>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:template match="td[position()='1']">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#136;&amp;#132;</xsl:text>
</xsl:template>

<xsl:template match="td[@align='right']">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#134;</xsl:text>
</xsl:template>

<xsl:template match="td[@align='center']">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#133;</xsl:text>
</xsl:template>

<xsl:template match="td">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#132;</xsl:text>
</xsl:template>

<!-- Tabell t01u2 -->
<xsl:template match="table[@class='t01u2']">
	<xsl:apply-templates mode="t01u2"/>
</xsl:template>

<xsl:template match="tr" mode="t01u2">
	<xsl:text>&#160;</xsl:text>
	<xsl:apply-templates mode="t01u2"/>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:template match="td[position()='1']" mode="t01u2">
	<xsl:value-of select="substring-before(., '-')"/>
	<xsl:text>&amp;#208;</xsl:text>
	<xsl:value-of select="substring-after(., '-')"/>
	<xsl:text>&amp;#135;&amp;#132;&#32;</xsl:text>		
</xsl:template>

<xsl:template match="td[position()>'1']" mode="t01u2">
	<xsl:value-of select="."/>
	<xsl:text>&amp;#134;</xsl:text>
</xsl:template>

<xsl:template match="td[.='-']" mode="t01u2">
	<xsl:text>&amp;#208;&amp;#133;&#32;</xsl:text>
</xsl:template>

<!-- Tabell t01u5 -->
<xsl:template match="table[@class='t01u5']">
	<xsl:apply-templates mode="t01u5"/>

	<xsl:text>&amp;#149;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#148;&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#128;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:template match="tr" mode="t01u5">
	<xsl:text>&#160;</xsl:text>
	<xsl:apply-templates mode="t01u5"/>

	<xsl:value-of select="$crlf"/>
</xsl:template>

<xsl:template match="td[position()='1']" mode="t01u5">
	<xsl:value-of select="."/>
	<xsl:value-of select="substring('              ', string-length(.))"/>

	<xsl:text>&amp;#136;&amp;#132;</xsl:text>
</xsl:template>

<xsl:template match="td[position()>'1']" mode="t01u5">
	<xsl:value-of select="substring('   ', string-length(.))"/>
	<xsl:value-of select="."/>

	<xsl:text>&amp;#134;</xsl:text>
</xsl:template>

<xsl:template match="td[.='-']" mode="t01u5">
	<xsl:text>&amp;#208;&amp;#133;</xsl:text>
</xsl:template>

<!-- Article author e-mail (in NTB used as signature) -->
<xsl:template match="tagline">
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#x99;</xsl:text>

	<xsl:value-of select="a"/>

	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
</xsl:template>

<!-- Pictures with Caption -->
<xsl:template match="body/body.content/media[@media-type='image']">
	<xsl:text>&amp;#x80;</xsl:text>
	<xsl:value-of select="$crlf"/>
	<xsl:text>Scanpix ID: </xsl:text>

	<xsl:value-of select="media-reference/@source"/>

	<xsl:value-of select="$crlf"/>

	<xsl:value-of select="user:MakeLineBreaks(media-caption)"/>
	<!--
	<xsl:choose>
		<xsl:when test="H�yde">
			<xsl:text> (H�ydebilde)</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text> (Breddebilde)</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	-->

	<xsl:value-of select="$crlf"/>
	<xsl:text>&amp;#x80;</xsl:text>
</xsl:template>

>>>>>>> ee1ac5a057562e95d3fc3b9add0412e9633c41eb
</xsl:stylesheet>