Imports System.Data.OleDb
Imports System.Text
Imports System.IO
Imports System.Web.Mail
Imports System.Configuration
Imports System.Diagnostics.Eventing.Reader
Imports System.Net.Mail
Imports System.ServiceProcess
Imports System.Web.UI.WebControls

Module ModulePublic
    Public Const TEXT_STARTED As String = "NTB_XmlXsltRoute Service Started"
    Public Const TEXT_STOPPED As String = "NTB_XmlXsltRoute Service Stopped"
    Public Const TEXT_INIT As String = "NTB_XmlXsltRoute Init finished"
    Public Const TEXT_PAUSE As String = "NTB_XmlXsltRoute Service Paused"
    Public Const TEXT_LINE As String = "----------------------------------------------------------------------------------"
    Public Const DATE_TIME As String = "yyyy-MM-dd HH:mm:ss"
    Public Const DATE_LOGFILE As String = "yyyy-MM-dd"

    Public MyEncoding As Encoding = Encoding.GetEncoding("iso-8859-1")
    Public TxtEncoding As Encoding = Encoding.GetEncoding(1252)

    Public ErrFilePath As String = ConfigurationManager.AppSettings("errFilePath")

    Public DeleteTempFiles As Boolean = UCase(ConfigurationManager.AppSettings("deleteTempFiles")) <> "NO"
    Public DatabaseOfflinePath As String = ConfigurationManager.AppSettings("databaseOfflinePath")
    Public TempPath As String = ConfigurationManager.AppSettings("tempPath")

    Public WatchInitialDelay As Integer = ConfigurationManager.AppSettings("watchInitialDelay") * 60 * 1000
    Public SmsFolder As String
    Public SmtpServer As String = ConfigurationManager.AppSettings("SmtpServer")

    Public PrimarySenderName As String = ConfigurationManager.AppSettings("primarySenderName")
    Public PrimarySenderAddress As String = ConfigurationManager.AppSettings("primarySenderAddress")

    Public SecondarySenderCriteria As String = ConfigurationManager.AppSettings("secondarySenderCriteria")
    Public SecondarySenderCriteriaValue1 As String = ConfigurationManager.AppSettings("secondarySenderCriteriaValue1")
    Public SecondarySenderCriteriaValue2 As String = ConfigurationManager.AppSettings("secondarySenderCriteriaValue2")
    Public SecondarySenderName As String = ConfigurationManager.AppSettings("secondarySenderName")
    Public SecondarySenderAddress As String = ConfigurationManager.AppSettings("secondarySenderAddress")

    Public sqlConnectionString As String = ConfigurationManager.AppSettings("SqlConnectionString")
    Public cn As New OleDbConnection(sqlConnectionString)

    Public Sub FillOneDataSet(ByRef dsDataSet As DataSet, ByVal strOfflineFile As String, ByVal strCommand As String, ByVal dataAdapter As OleDbDataAdapter, ByVal command As OleDbCommand, Optional ByVal strGroupListID As String = "")
        Dim strFile As String = DatabaseOfflinePath & "\" & strOfflineFile & strGroupListID & ".xml"
        If cn.State = ConnectionState.Open Then
            command.CommandText = strCommand
            Try
                dataAdapter.Fill(dsDataSet)
                dsDataSet.WriteXml(strFile, XmlWriteMode.WriteSchema)
            Catch e As Exception
                WriteErr(ErrFilePath, "Feil i FillOneDataSet: ", e)
                dsDataSet.ReadXml(strFile, XmlReadMode.ReadSchema)
            End Try
        Else
            dsDataSet.ReadXml(strFile, XmlReadMode.ReadSchema)
        End If
    End Sub

    Public Sub WriteErr(ByRef strLogPath As String, ByRef strMessage As String, ByRef e As Exception)
        Dim strLine As String

        On Error Resume Next

        strLine = "Error: " & Format(Now, DATE_TIME) & vbCrLf
        strLine &= strMessage & vbCrLf
        strLine &= e.Message & vbCrLf
        strLine &= e.Source & vbCrLf
        strLine &= e.StackTrace

        Dim strFile As String = strLogPath & "\Error-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, MyEncoding)
        w.WriteLine(strLine)
        w.WriteLine(TEXT_LINE)
        w.Flush()  ' update underlying file
        w.Close()  ' close the writer and underlying file

        'If errorMailAddress <> "" Then
        '    SendErrMail(strLine)
        'End If
    End Sub

    Public Sub WriteLog(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim strLine As String

        strLine = Format(Now, DATE_TIME) & ": " & strMessage

        Dim w As StreamWriter = New StreamWriter(strFile, True, MyEncoding)
        w.WriteLine(strLine)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteLogNoDate(ByRef strLogPath As String, ByRef strMessage As String)
        Dim strFile As String = strLogPath & "\Log-" & Format(Now, DATE_LOGFILE) & ".log"
        Dim w As New StreamWriter(strFile, True, MyEncoding)
        w.WriteLine(strMessage)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
    End Sub

    Public Sub WriteFile(ByRef strFileName As String, ByRef strContent As String, Optional ByVal append As Boolean = False, Optional ByVal encoding As String = "")
        Dim utf8 As Encoding = System.Text.Encoding.UTF8
        Dim latin As Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1")
        Dim iso As Encoding = System.Text.Encoding.GetEncoding(1252)

        Dim enc As Encoding

        If encoding = String.Empty Then
            enc = iso
        Else
            If encoding = "UTF-8" Then
                enc = utf8
            Else
                ' enc = New System.Text.Encoding(System.Text.Encoding.GetEncoding(encoding))
                enc = latin

            End If

        End If

        'If encoding = "" Then : enc = TxtEncoding

        'Else : enc = System.Text.Encoding.GetEncoding(encoding)
        'End If

        Dim w As New StreamWriter(strFileName, append, enc)
        w.WriteLine(strContent)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file
        w.Dispose()
    End Sub

    Public Function ReadFile(ByVal strFileName As String) As String
        ' Simple File reader returns File as String
        Dim sr As New StreamReader(strFileName, TxtEncoding)    ' File.OpenText("log.txt")
        ReadFile = sr.ReadToEnd
        sr.Close()
    End Function

    Public Sub MakePath(ByVal strPath As String)
        'If Not Directory.Exists(strPath) Then
        Directory.CreateDirectory(strPath)
        'End If
    End Sub

    Public Function MakeSubDirDate(ByRef strFilePath As String, ByVal dtTimeStamp As DateTime) As String
        Dim strFile As String = Path.GetFileNameWithoutExtension(strFilePath)
        Dim strPath As String = Path.GetDirectoryName(strFilePath)

        'strSub &= dtTimeStamp.Year & "\" & dtTimeStamp.Month & "-" & dtTimeStamp.Day
        Dim strSub As String = Format(dtTimeStamp, "yyyy-MM") & "\" & Format(dtTimeStamp, "yyyy-MM-dd")
        strPath = strPath & "\" & strSub & "\"
        If Not Directory.Exists(strPath) Then
            Directory.CreateDirectory(strPath)
        End If

        Return strPath & Path.GetFileName(strFilePath)

    End Function

    'Check if SMTP is running
    Function SmtpRunning(ByVal strLogPath As String) As Boolean

        Dim ret As Boolean = False

        ' check if it is running
        Dim localServices As ServiceController() = ServiceController.GetServices()

        ' Looping over the services
        For Each service As ServiceController In localServices
            If Not String.IsNullOrEmpty(service.ServiceName) Then
                If service.ServiceName = "SMTPSVC" Then
                    If service.Status.ToString().ToUpper() = "RUNNING" Then
                        ret = True
                    End If
                End If
            End If
        Next

        If ret = False Then
            WriteLog(strLogPath, "Service IS NOT running")
        Else
            WriteLog(strLogPath, "Service IS running")
        End If

        Return ret

        'Dim computername As String = Environment.MachineName
        'Dim objComp = GetObject("WinNT://" & computername & ",computer")
        'Dim objService = objComp.GetObject("Service", "SMTPSVC")
        'ret = (objService.Status = 4)

        ' WriteLog(strLogPath, "Service is running")
        ' ret = True
        ' End If


        Return ret
    End Function

    Sub SendErrMail(ByVal strMessage As String, ByVal address As String, Optional ByVal subject As String = "Error from NTB XmlRouteService")

        Dim mySmtpServer As String = ConfigurationManager.AppSettings("SmtpServer")
        Dim smtpServerPort As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("SmtpServerPort"))

        Dim smtpClient As SmtpClient = New SmtpClient(mySmtpServer, smtpServerPort)

        Dim mailMessage As Net.Mail.MailMessage = New Net.Mail.MailMessage()
        mailMessage.Subject = subject
        mailMessage.Body = strMessage
        mailMessage.IsBodyHtml = False
        mailMessage.To.Add(New MailAddress(address))
        mailMessage.From = New MailAddress(address)

        smtpClient.Send(mailMessage)

    End Sub

End Module
