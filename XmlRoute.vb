Imports System.Collections.Generic
Imports System.Configuration
Imports System.Xml
Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Imports System.Linq
Imports System.Net.Configuration
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Text
Imports Microsoft.Web.Administration


Public Class XmlRoute

    Private WithEvents _timer As Timers.Timer = New Timers.Timer()
    Private WithEvents _fileSystemWatcher As FileSystemWatcher = New FileSystemWatcher()

    Private WithEvents WatchTimer As Timers.Timer = New Timers.Timer()
    Private WithEvents DelayTimer As Timers.Timer = New Timers.Timer()

    Private ReadOnly _mutex As Threading.Mutex

    Const TextInit As String = "NTB_XmlXsltRoute 'Class XmlRoute' Init finished"

    Private Const Ok = 1
    Private Const ErrXml = 2
    Private Const ErrDelete = 3

    Enum DoneStatus
        Delete = 1
        Move = 2
        CopyDelete = 3
        Leave = 4
    End Enum

    Enum InteruptType
        Timer = 1
        Ntfs = 2
        NtfsTimer = 3
    End Enum

    'Win API function Sleep
    Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

    'Old deleted: 20.05.2003 RoV
    'Public htXsltProcs As Hashtable
    'Private htXsltProcs As Hashtable
    'Private strXsltPath As String
    'Private htArgumentLists As Hashtable = New Hashtable()
    'Private dsXsltJobs As DataSet
    ' Private objMain As FormTest
    'Public objXchgReplace As XchgReplace

    'Public class variabler: 
    Public HtDistKodeSatAviser As Hashtable
    Public BBusy As Boolean
    Public IntGroupListId As Integer

    'Private
    Private ReadOnly _objSniff As New SniffSearch()
    Private _logFilePath As String
    Private ReadOnly _bEnableRaisingEvents As Boolean
    Private ReadOnly _bTimerEnabled As Boolean
    Private _bEnableRaisingEventsPause As Boolean
    Private _bTimerEnabledPause As Boolean

    'Internal class variabler: 
    Private ReadOnly _htTempFiles As Hashtable = New Hashtable()
    Private ReadOnly _htMailCache As Hashtable = New Hashtable()
    Private ReadOnly _htErrorStates As Hashtable = New Hashtable()

    Private _dsCustomerJob As DataSet
    Private _dsCustomer As DataSet
    Private _dsCustomerDistKode As DataSet
    Private _dsWatchFolder As DataSet
    Private _dsDelayFolder As DataSet

    Private ReadOnly _errorSmsNumber As String
    Private ReadOnly _errorMailAddress As String

    Private ReadOnly _strInputPath As String
    Private ReadOnly _strInputFilter As String
    Private ReadOnly _strDonePath As String

    Private ReadOnly _strDelayPath As String
    Private ReadOnly _strDefOutPath As String

    Private ReadOnly _strErrorPath As String

    Private ReadOnly _intDoneStatusId As Integer
    Private ReadOnly _intSleepSec As Integer
    Private ReadOnly _intPollingInterval As Integer
    Private ReadOnly _intInteruptType As InteruptType

    Private ReadOnly _isXmlInput As Boolean

    'Default constructor
    Public Sub New()
    End Sub

    'Constructor
    Public Sub New(ByVal cn As OleDbConnection, ByVal rowGroupList As DataRow, ByVal strLogFilePath As String, ByVal strDatabaseOfflinePath As String) ', ByVal xsltPath As String)

        IntGroupListId = rowGroupList("GroupListID") & ""

        _mutex = New Threading.Mutex(False, "XMLROUTE_" & IntGroupListId & "_LOGFILE")

        _logFilePath = strLogFilePath & "\GroupList" & IntGroupListId
        OfflinePath = strDatabaseOfflinePath
        _strDonePath = rowGroupList("DonePath") & ""
        _intDoneStatusId = rowGroupList("DoneStatusID") & ""
        _intSleepSec = rowGroupList("SleepInterval") & ""
        _strInputPath = rowGroupList("InputPath") & ""
        _intPollingInterval = rowGroupList("PollingInterval") & ""
        _intInteruptType = rowGroupList("InteruptType") & ""
        _strErrorPath = rowGroupList("ErrorPath") & ""
        _isXmlInput = rowGroupList("IsXmlInput") & ""
        _strInputFilter = rowGroupList("InputFilter") & ""
        _errorSmsNumber = rowGroupList("ErrorSMSNumber") & ""
        _errorMailAddress = rowGroupList("ErrorEmail") & ""

        'Folders folder for holded messages
        _strDefOutPath = rowGroupList("DefCustomerRootPath") & ""
        _strDelayPath = rowGroupList("DelayPath") & ""

        FillDataSets(cn)
        FillHashtables()

        ' Creating out directories
        CreateOutDirectories()

        ' First we must create users ...
        CreateUsers()

        ' ... then we can create virtual directories
        CreateFtpVirtualDirs()

        'Surveillance
        DelayTimer.Interval = _intPollingInterval * 1000
        DelayTimer.Enabled = True

        'Surveillance
        WatchTimer.Interval = WatchInitialDelay
        WatchTimer.Enabled = True

        'File handling
        _timer.Interval = _intPollingInterval
        _fileSystemWatcher.Path = _strInputPath
        _fileSystemWatcher.Filter = _strInputFilter

        Select Case _intInteruptType
            Case InteruptType.Timer
                _bTimerEnabled = True
            Case InteruptType.Ntfs
                _bEnableRaisingEvents = True
            Case InteruptType.NtfsTimer
                _bTimerEnabled = True
                _bEnableRaisingEvents = True
        End Select
        WriteLog(_logFilePath, TextInit)
    End Sub

    Public Property OfflinePath As String

    Private Sub WatchTimer_Elapsed(ByVal sender As Object, ByVal e As Timers.ElapsedEventArgs) Handles WatchTimer.Elapsed
        WatchTimer.Enabled = False
        _mutex.WaitOne()

        Try
            If SmtpRunning(_logFilePath) Then WatchFolders()
        Catch ex As Exception
            WriteErr(_logFilePath, "Feil ved mappeoverv�kning.", ex)
        End Try

        _mutex.ReleaseMutex()
        WatchTimer.Interval = _intPollingInterval * 1000
        WatchTimer.Enabled = True
    End Sub

    Private Sub DelayTimer_Elapsed(ByVal sender As Object, ByVal e As Timers.ElapsedEventArgs) Handles DelayTimer.Elapsed
        DelayTimer.Enabled = False
        _mutex.WaitOne()

        Try
            MoveDelayed()
        Catch ex As Exception
            WriteErr(_logFilePath, "Feil ved flytting av hold-filer.", ex)
        End Try

        _mutex.ReleaseMutex()
        DelayTimer.Interval = _intPollingInterval * 1000
        DelayTimer.Enabled = True
    End Sub

    Private Sub Timer1_Elapsed(ByVal sender As System.Object, ByVal e As Timers.ElapsedEventArgs) Handles _timer.Elapsed
        BBusy = True
        _timer.Enabled = False
        _fileSystemWatcher.EnableRaisingEvents = False
        _mutex.WaitOne()

        _timer.Interval = _intPollingInterval * 1000
        DoAllInputFiles()

        _mutex.ReleaseMutex()
        _timer.Enabled = _bTimerEnabledPause
        _fileSystemWatcher.EnableRaisingEvents = _bEnableRaisingEventsPause
        BBusy = False
    End Sub

    Private Sub FileSystemWatcher1_Created(ByVal sender As System.Object, ByVal e As FileSystemEventArgs) Handles _fileSystemWatcher.Created
        BBusy = True
        _fileSystemWatcher.EnableRaisingEvents = False
        _timer.Enabled = False
        _mutex.WaitOne()

        'Wait some seconds for last inputfile file to be written by external program, before reading
        Sleep(_intSleepSec * 1000)

        'DoOneInputFile(e.FullPath)
        'Do new files which might have appeared during DoOneInputFile
        DoAllInputFiles()

        _mutex.ReleaseMutex()
        _fileSystemWatcher.EnableRaisingEvents = _bEnableRaisingEventsPause
        _timer.Enabled = _bTimerEnabledPause
        BBusy = False
    End Sub

    Public Sub Start()
        _bTimerEnabledPause = _bTimerEnabled
        _timer.Enabled = _bTimerEnabled
        _bEnableRaisingEventsPause = _bEnableRaisingEvents
        _fileSystemWatcher.EnableRaisingEvents = _bEnableRaisingEvents

        'Do any files waiting in Input Path when starting program
        'Must do this for DeleteFIleSync to work properly - Nope fixed Deletefilesync :)
        DoAllInputFiles()

        _timer.Interval = 2000
        _timer.Enabled = True
    End Sub

    Public Sub Pause()
        _bTimerEnabledPause = False
        _timer.Enabled = False
        _bEnableRaisingEventsPause = False
        _fileSystemWatcher.EnableRaisingEvents = False
    End Sub

    Private Sub DoAllInputFiles() 'ByRef rowGroupList As DataRow, ByRef intCount As Integer)
        Dim arrFileList() As String
        Dim strInputFile As String

        ' Read all files in folder
        Try
            arrFileList = Directory.GetFiles(_strInputPath, _strInputFilter)
        Catch e As Exception
            WriteErr(_logFilePath, "Feil input directory: " & _strInputPath, e)
            Exit Sub
        End Try

        'Wait some seconds for last inputfile file to be written by external program, before reading
        Sleep(_intSleepSec * 1000)
        For Each strInputFile In arrFileList
            DoOneInputFile(strInputFile)
            ' Windows.Forms.Application.DoEvents()
        Next
    End Sub

    Private Sub DoOneInputFile(ByVal strInputFile As String)

        Try
            WriteLogNoDate(_logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
            WriteLog(_logFilePath, "Start: " & strInputFile)
            WriteLogNoDate(_logFilePath, "---------------------------------------------------------------------------------------------------------------------------")

            Dim returnval As Integer = RouteFile(strInputFile, _strDonePath, _intDoneStatusId)
            WriteLog(_logFilePath, "Returning from RouteFile: " & returnval)

        Catch e As Exception
            Try
                WriteErr(_logFilePath, "Feilet: " & strInputFile, e)
                Dim strOutPutFile As String = _strErrorPath & "\" & Path.GetFileName(strInputFile)
                File.Copy(strInputFile, strOutPutFile)
                File.Delete(strInputFile)
            Catch
            End Try
        End Try

    End Sub

    Private Function WaitForFile(ByVal fullPath As String, ByVal mode As FileMode, ByVal access As FileAccess, ByVal share As FileShare) As FileStream
        For numTries As Integer = 0 To 10
            Try
                Dim fs As FileStream = New FileStream(fullPath, mode, access, share)

                fs.ReadByte()

                fs.Seek(0, SeekOrigin.Begin)

                Return fs
            Catch ex As Exception
                Threading.Thread.Sleep(50)
            End Try
        Next

        Return Nothing
    End Function

    Private Function RouteFile(ByVal strFilename As String, ByVal strLocalDonePath As String, ByVal intLocalDoneStatusId As DoneStatus) As Integer
        Dim xmlDoc As XmlDocument = New XmlDocument()
        'Dim xpathDoc As XPath.XPathDocument
        'Dim bCheckOk As Boolean
        Dim rowCustomer As DataRow
        Dim strTempFile As String
        Dim strDoneFile As String
        Dim strDisKode As String


        If _isXmlInput Then

            Try
                WriteLog(_logFilePath, String.Format("We are do compute {0}", strFilename))

                ' Loading the file

                ' Is file ready to be read
                Dim fs As FileStream = Me.WaitForFile(strFilename, FileMode.Open, FileAccess.Read, FileShare.Read)

                If fs.CanRead Then
                    ' xmlDoc.Load(strFilename)

                    Dim sr As StreamReader = New StreamReader(strFilename, Encoding.GetEncoding("ISO-8859-1"))
                    Dim content As String = sr.ReadToEnd()
                    sr.Close()
                    sr.Dispose()

                    xmlDoc.LoadXml(content)
                    _objSniff.SetXmlText(xmlDoc)
                    
                    WriteLog(_logFilePath, String.Format("We have loaded and closed the file {0}", strFilename))

                Else
                    WriteLog(_logFilePath, "Could not load file... " & strFilename)
                    Throw New Exception("Could not load file!")
                End If

                fs.Dispose()

                ' deleting file just to find out where in the f... world it is loading it again
                'Try
                '    Dim fi As FileInfo = New FileInfo(strFilename)
                '    fi.Delete()
                'Catch ex As Exception
                '    WriteErr(_logFilePath, "Something happened while trying to delete file", ex)
                'End Try

            Catch
                Return ErrXml
            End Try

            strDisKode = GetArticleDistKode(xmlDoc)

            For Each rowCustomer In _dsCustomer.Tables(0).Rows
                If CheckForCopy(xmlDoc, rowCustomer, strDisKode) Then
                    Dim boolReturn As Boolean = CopyTrans(strFilename, rowCustomer)

                    If boolReturn = True Then
                        ' WriteLog(_logFilePath, "Copied " & strFilename)
                    End If
                Else
                    WriteLogNoDate(_logFilePath, "                     Ikke til: " & rowCustomer("CustomerName") & ", XpathDef: " & rowCustomer("XpathName"))
                End If
            Next


        Else
            'For text files then no Check before copy, just copy all files to all customers
            For Each rowCustomer In _dsCustomer.Tables(0).Rows
                ' CopyTrans(strFilename, rowCustomer)
                Dim boolReturn As Boolean = CopyTrans(strFilename, rowCustomer)
                If boolReturn = True Then
                    WriteLog(_logFilePath, "Kopiert Til: " & rowCustomer("CustomerName"))
                    ' WriteLog(_logFilePath, "Copied " & strFilename)
                End If
            Next
        End If

        'Delete Temp Files and cache
        If DeleteTempFiles Then
            For Each strTempFile In _htTempFiles.Values
                File.Delete(strTempFile)
            Next
        End If
        _htTempFiles.Clear()
        _htMailCache.Clear()

        Try
            Dim strStatus As String
            Dim fi As FileInfo = New FileInfo(strFilename)

            strDoneFile = strLocalDonePath & Path.DirectorySeparatorChar & fi.Name ' Path.GetFileName(strFilename)

            'Create datesub
            strDoneFile = MakeSubDirDate(strDoneFile, fi.LastWriteTime) 'File.GetLastWriteTime(strFilename)

            Select Case intLocalDoneStatusId
                Case DoneStatus.CopyDelete
                    fi.CopyTo(strDoneFile, True)
                    fi.Delete()

                    ' File.Copy(strFilename, strDoneFile, True)
                    ' File.Delete(strFilename)
                    strStatus = "CopyDeleted to: " & strDoneFile
                Case DoneStatus.Move
                    fi.MoveTo(strDoneFile)
                    ' File.Move(strFilename, strDoneFile)
                    strStatus = "Moved to: " & strDoneFile
                Case DoneStatus.Delete
                    fi.Delete()
                    ' File.Delete(strFilename)
                    strStatus = "Deleted: " & strFilename
                Case DoneStatus.Leave
                    strStatus = "Not Deleted: " & strFilename
                    'do nothing
            End Select
            WriteLogNoDate(_logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
            WriteLog(_logFilePath, "Routing finished: " & strStatus)

        Catch e As Exception

            WriteErr(_logFilePath, "Deleting failed: ", e)
            If Not IsNothing(e.InnerException) Then
                WriteErr(_logFilePath, "Inner exception: ", e.InnerException)
            End If
            Return ErrDelete

        End Try

        Return Ok

    End Function

    'Add local user for FTP fetch
    Private Sub CreateUsers()

        Dim row As DataRow

        Dim computername As String = Environment.MachineName
        Dim objComputer As Object
        Dim objGroup1 As Object
        Dim objGroup2 As Object
        Dim objUser As Object

        objComputer = GetObject("WinNT://" & computername)
        objGroup1 = GetObject("WinNT://" & computername & "/Users")
        objGroup2 = GetObject("WinNT://" & computername & "/FTP Users")


        For Each row In _dsCustomer.Tables(0).Rows


            Dim ftpGet As Integer
            Dim name As String
            Dim password As String

            Try
                ftpGet = row("FtpTypeID")
                name = row("Username")
                password = row("Password")
            Catch
            End Try

            If name <> "" Then
                WriteLog(_logFilePath, "Creating user: " & name)
            Else
                WriteLog(_logFilePath, "Name is not defined!")
            End If


            If ftpGet = 1 And name <> "" Then
                'Execute shell VBScript
                Try
                    objUser = objComputer.GetObject("User", name)
                Catch
                    objUser = objComputer.Create("User", name)
                End Try

                Try
                    objUser.SetPassword(password)
                    objUser.SetInfo()

                    Try
                        objGroup1.Remove("WinNT://" & computername & "/" & name)
                        objUser.SetInfo()
                    Catch
                    End Try

                    Try
                        objGroup2.Add("WinNT://" & computername & "/" & name)
                        objUser.SetInfo()
                    Catch
                    End Try

                Catch e As Exception
                    WriteErr(_logFilePath, "Failed to create user!", e)
                End Try
            End If

        Next

    End Sub

    'Add vdirs for FTP get
    Private Sub CreateFtpVirtualDirs()

        Dim row As DataRow
        Dim ok As Boolean = True

        ' Dim conn As SWbemLocatorClass = New SWbemLocatorClass()
        ' Dim prov As SWbemServices

        ' Dim node, n2, n3 As SWbemObject
        ' Dim ret As SWbemObjectPath

        ' This is how you create a virtual directory with IIS manager dlls
        Dim iisManager As ServerManager = New ServerManager()

        If ok Then
            For Each row In _dsCustomer.Tables(0).Rows

                Dim ftpGet As Integer
                Dim name As String = String.Empty
                Dim folder As String

                Try
                    ftpGet = row("FtpTypeID")
                    name = row("Username")
                    folder = row("DefOutPath")
                Catch
                End Try

                If ftpGet = 1 And name <> "" Then

                    Try

                        Dim site As Microsoft.Web.Administration.Site = iisManager.Sites("Default FTP Site")
                        Dim app As Application = site.Applications("/")

                        ' n2 = prov.Get("IIsFtpVirtualDirSetting='MSFTPSVC/1/ROOT/" & name & "'")
                        WriteLog(_logFilePath, "About to create name: " & name & " with folder: " & folder)

                        Dim foundDirectory As IEnumerable(Of VirtualDirectory) = From vdir In app.VirtualDirectories
                                  Where vdir.Path = "/" & name
                                  Select vdir

                        If foundDirectory.Any() Then
                            WriteLog(_logFilePath, "Virtual Directory existed")
                            Continue For
                        End If

                        app.VirtualDirectories.Add("/" & name, folder)
                        ' Committing changes
                        WriteLog(_logFilePath, "CommitChanges")
                        iisManager.CommitChanges()

                        WriteLog(_logFilePath, "Done creating /" & name & " physical path: " & folder)

                        WriteLog(_logFilePath, "Creating user FTP Authorization Rules")
                        Me.SetFtpAuthorizationRules(name)
                    Catch a As Exception
                        ' n2 = prov.Get("IIsFtpVirtualDirSetting")
                        WriteErr(_logFilePath, "An error occured while creating directory", a)
                    End Try

                    'node = n2.SpawnInstance_()

                    'node.Properties_.Item("Name").Value = "MSFTPSVC/1/ROOT/" & name
                    'node.Properties_.Item("Path").Value = folder
                    'node.Properties_.Item("AccessRead").Value = True
                    'node.Properties_.Item("AccessWrite").Value = True

                    'node.Put_()

                End If
            Next
        End If
    End Sub

    Private Sub SetFtpAuthorizationRules(ByVal user As String)
        Dim serverManager As ServerManager = New ServerManager
        Dim config As Microsoft.Web.Administration.Configuration = serverManager.GetApplicationHostConfiguration

        Dim authorizationSection As Microsoft.Web.Administration.ConfigurationSection = config.GetSection("system.ftpServer/security/authorization", "Default FTP Site/" & user)
        Dim authorizationCollection As Microsoft.Web.Administration.ConfigurationElementCollection = authorizationSection.GetCollection

        If authorizationCollection Is Nothing Then
            ' create section - now how do we do that?
            Return
        End If


        Dim addElement As Microsoft.Web.Administration.ConfigurationElement = authorizationCollection.CreateElement("add")

        addElement("accessType") = "Allow"
        addElement("users") = user
        addElement("permissions") = "Read,Write"

        authorizationCollection.Add(addElement)
        serverManager.CommitChanges()
    End Sub

    Private Sub SetFtpAuthorizationRulesFromOldMasterMaybeAddThis(ByVal name As String)
        ' Create a new Server Manger
        Dim iisManager As ServerManager = New ServerManager

        ' We are checking if a role exists
        Dim addUserRestrictions = True

        ' Getting the configuration
        Dim config As Microsoft.Web.Administration.Configuration = iisManager.GetApplicationHostConfiguration

        ' Get the section
        Dim authorizationSection As Microsoft.Web.Administration.ConfigurationSection = config.GetSection("system.ftpServer/security/authorization", "Default FTP Site/" & name)

        ' Get the collection
        Dim authorizationCollection As Microsoft.Web.Administration.ConfigurationElementCollection = authorizationSection.GetCollection

        ' Looping over to find the role
        For Each configurationElement As Microsoft.Web.Administration.ConfigurationElement In authorizationCollection
            ' Looping over the attributes for this element 
            ' It is probably a better way to do this, but as long as this works....
            If configurationElement.Attributes.Count = 0 Then
                Continue For
            End If

            For Each configurationAttribute As Microsoft.Web.Administration.ConfigurationAttribute In configurationElement.Attributes

                ' If we find the role, we shall not add the role
                If configurationAttribute.Name.ToLower() = "users" And configurationAttribute.Value.ToString() = name Then
                    addUserRestrictions = False
                End If
            Next

        Next

        ' We are adding the role
        If addUserRestrictions = True Then
            Dim addElement As Microsoft.Web.Administration.ConfigurationElement = authorizationCollection.CreateElement("add")
            ' We are defining access type allow
            addElement("accessType") = "Allow"

            ' Adding the role FTP Users to the attribute roles
            addElement("users") = name

            ' Users in this role can both read and write (which means delete the folders they have picked up
            addElement("permissions") = "Read, Write"

            ' We are adding the element
            authorizationCollection.Add(addElement)

            ' We are committing the changes
            iisManager.CommitChanges()
        End If

    End Sub

    Private Sub CreateOutDirectories()

        Dim strOutPath As String
        Dim bEmail As Boolean
        Dim delay As Integer

        Dim row As DataRow

        'Make paths
        For Each row In _dsCustomerJob.Tables(0).Rows
            strOutPath = row("OutPath")

            bEmail = row("Email")

            If strOutPath <> "" And Not bEmail Then
                Directory.CreateDirectory(strOutPath)
            End If

            delay = row("PushDelay")
            If delay > 0 Then
                'Calculate temp dir for customer
                Directory.CreateDirectory(strOutPath.Replace(_strDefOutPath, _strDelayPath))
            End If

        Next

        Directory.CreateDirectory(_strDonePath)
        Directory.CreateDirectory(_strErrorPath)
        Directory.CreateDirectory(_strInputPath)
        Directory.CreateDirectory(_logFilePath)
    End Sub

    Private Sub FillDataSets(ByRef cn As OleDbConnection)
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand = New OleDbCommand()

        dataAdapter.SelectCommand = command
        command.Connection = cn

        _dsCustomerJob = New DataSet()
        _dsCustomer = New DataSet()
        _dsCustomerDistKode = New DataSet()
        _dsWatchFolder = New DataSet()
        _dsDelayFolder = New DataSet()

        FillOneDataSet(_dsCustomer, "dsCustomer", "SELECT * FROM GetCustomer WHERE GroupListID=" & IntGroupListId, dataAdapter, command, IntGroupListId)
        FillOneDataSet(_dsCustomerJob, "dsCustomerJob", "SELECT * FROM GetCustomerJob WHERE GroupListID=" & IntGroupListId, dataAdapter, command, IntGroupListId)
        FillOneDataSet(_dsCustomerDistKode, "dsCustomerDistKode", "SELECT * FROM CustomerDistKode", dataAdapter, command, IntGroupListId)
        FillOneDataSet(_dsWatchFolder, "dsWatchFolder", "SELECT * FROM GetWatchFolders WHERE GroupListID=" & IntGroupListId, dataAdapter, command)
        FillOneDataSet(_dsDelayFolder, "dsDelayFolder", "SELECT * FROM GetDelayFolders WHERE GroupListID=" & IntGroupListId, dataAdapter, command)

        ' dataAdapter = Nothing
        ' command = Nothing

    End Sub

    Private Sub FillHashtables()
        Dim row As DataRow

        For Each row In _dsCustomer.Tables(0).Rows
            Dim strSniffText As String = row("SniffText") & ""
            If strSniffText <> "" Then
                Dim customerId As Integer = row("customerID")
                _objSniff.SetRegexCustomer(customerId, strSniffText, _logFilePath)
            End If
        Next

        'Dim xsltFile As String
        'htXsltProcs = New Hashtable()

        ''Lag XSLT prosessors for hver XSLT-fil og legg i Hashtable
        'For Each row In dsXsltJobs.Tables(0).Rows
        '    jobDefID = row("JobDefID")
        '    xsltFile = row("XsltFile") & ""
        '    If xsltFile <> "" Then
        '        Dim xsltProc As Xsl.XslTransform = New Xsl.XslTransform()
        '        Try
        '            xsltProc.Load(strXsltPath & "\" & xsltFile)
        '            htXsltProcs.Item(jobDefID) = xsltProc
        '        Catch e As Xsl.XsltException
        '            WriteErr(logFilePath, "Feil i lasting av XSLT file: " & xsltFile, e)
        '        End Try
        '    End If
        'Next

        ''Lag XsltArgumentList for hver Kunde og legg i Hashtable
        'Dim xslParam As Xsl.XsltArgumentList = New Xsl.XsltArgumentList()
        'For Each row In dsCustomerJob.Tables(0).Rows
        '    Dim strParam As String = row("XsltParamsCust") & ""
        '    If strParam <> "" Then
        '        Dim customerID As Integer = row("customerID")
        '        jobDefID = row("jobDefID")

        '        Dim arrParams As String() = Split(strParam, vbCrLf)
        '        For Each strParam In arrParams
        '            Dim strParamValue As String = row(strParam) & ""
        '            xslParam.AddParam(strParam, "", strParamValue)
        '        Next
        '        htArgumentLists.Add(customerID & "-" & jobDefID, xslParam)
        '    End If
        'Next
    End Sub

    Private Function CheckForCopy(ByRef xmlDoc As XmlDocument, ByRef rowCustomer As DataRow, ByVal strDisKode As String) As Boolean
        Dim strXpath As String = rowCustomer("XPath") & ""
        Dim customerId As String

        If strXpath = "" Then
            'Tom xpath gir kopiering uansett
            Return True
        End If

        If IsCustomerDistKode(rowCustomer, strDisKode) Then
            'Hvis meldingen innheolder Distribusjonkode eller avisens kode, kopier
            Return True
        End If

        If IsAvisDistKode(strDisKode) Then
            'Hvis meldingen finnes med en aviskode, ikke kopier
            Return False
        End If

        'Check xpath and sniff if given
        customerId = rowCustomer("CustomerID")
        If CheckXpath(xmlDoc, rowCustomer) And _objSniff.Find(customerId) <> 0 Then
            Return True
        End If

        '' At last if not found in other methods try Sniff search:
        'If objSniff.Find(customerId) > 0 Then
        '    Return True
        'End If

        Return False

    End Function

    Private Function CheckXpath(ByRef xmlDoc As XmlDocument, ByRef rowCustomer As DataRow) As Boolean
        Dim nodeList As XmlNodeList
        Dim strCompare As String
        Dim intCompareHit As Integer
        Dim intHits As Integer
        Dim bFound As Boolean

        strCompare = rowCustomer("compare")
        intCompareHit = rowCustomer("hits")
        Dim strXpath As String = rowCustomer("XPath") & ""

        Try
            nodeList = xmlDoc.SelectNodes(strXpath)
            intHits = nodeList.Count
        Catch e As Exception
            WriteErr(_logFilePath, "Feil i XPath: " & strXpath, e)
            Return False
        End Try

        bFound = False
        Select Case strCompare
            Case "gt"
                If intHits > intCompareHit Then
                    bFound = True
                End If
            Case "eq"
                If intHits = intCompareHit Then
                    bFound = True
                End If
            Case "lt"
                If intHits < intCompareHit Then
                    bFound = True
                End If
            Case "ne"
                If intHits <> intCompareHit Then
                    bFound = True
                End If
        End Select
        Return bFound
    End Function

    Private Function GetArticleDistKode(ByRef xmlDoc As XmlDocument) As String
        Try
            Dim strDistKode As String = xmlDoc.SelectSingleNode("/nitf/head/meta[@name='NTBDistribusjonsKode']/@content").Value
            Return UCase(strDistKode)
        Catch
            Return "ZZZ"
        End Try
    End Function

    Private Function IsCustomerDistKode(ByRef rowCustomer As DataRow, ByVal strDisKode As String) As Boolean
        If strDisKode = "" Then Return True
        Try
            Dim custDistKode As String = rowCustomer("Distkode") & ""
            If UCase(custDistKode) = strDisKode Then
                Return True
            End If

            Dim intCustomerId As String = rowCustomer("CustomerId")
            Dim row As DataRow
            For Each row In _dsCustomerDistKode.Tables(0).Select("CustomerId=" & intCustomerId)
                If strDisKode = UCase(row("DistKode")) Then Return True
            Next
        Catch
        End Try

        Return False
    End Function

    Private Function IsAvisDistKode(ByVal strDisKode As String) As Boolean
        Try
            If strDisKode = "" Then Return False
            Dim strTest = HtDistKodeSatAviser(strDisKode) & ""
            If strTest <> "" Then
                Return True
            Else
                Return False
            End If
        Catch
        End Try

        Return False
    End Function

    Private Function CopyTrans(ByVal strFilename As String, ByRef rowCustomer As DataRow, Optional ByVal xmlDoc As XmlDocument = Nothing) As Boolean
        Dim customerId As Integer
        Dim delay As Integer
        Dim jobTypeId As Integer
        Dim jobDefId As Integer
        Dim strOutPath, strSuffix As String
        Dim tmpFile As String
        Dim row As DataRow
        Dim rowsJobs As Array
        Dim xsltFile As String
        Dim bEMail As Boolean
        Dim encoding As String
        Dim rssOutput As Boolean
        Dim maxItemsRss As Integer

        'Select all job rows for one customer
        customerId = rowCustomer("CustomerId")
        delay = rowCustomer("PushDelay")
        rowsJobs = _dsCustomerJob.Tables(0).Select("CustomerId = " & customerId)

        For Each row In rowsJobs
            jobTypeId = row("JobTypeID")
            strOutPath = row("OutPath") & ""
            strSuffix = row("Suffix") & ""
            jobDefId = row("JobDefID")
            xsltFile = row("XsltFile") & ""
            bEMail = row("EMail")
            encoding = row("Encoding") & ""
            Try
                rssOutput = row("RSSOutput")
                maxItemsRss = row("MaxItemsRSS")
            Catch
            End Try
            If maxItemsRss = 0 Then
                maxItemsRss = 30
            End If

            Dim strTempDoc As String = ""
            Dim bXsltWithParams As Boolean = False

            If strSuffix = "" Then
                strSuffix = Path.GetExtension(strFilename)
            Else
                strSuffix = "." & strSuffix
            End If

            If jobTypeId = 1 Then
                'Copy Files
                tmpFile = strFilename
            Else
                'Transform Files
                Dim xchgBeforeId As Integer = Val(row("XchgBeforeID") & "")
                Dim xchgAfterId As Integer = Val(row("XchgAfterID") & "")

                ' Hent temp-file i tempfile cache
                tmpFile = _htTempFiles(jobDefId)

                ' Hvis fila ikke er konvertert allerede
                If tmpFile = "" Then
                    tmpFile = TempPath & "\JobDefID-" & jobDefId & "_" & Path.GetFileNameWithoutExtension(strFilename) & strSuffix

                    'Find and Replace before XSLT
                    If xchgBeforeId > 0 Then
                        strTempDoc = DoXchg(xchgBeforeId, strFilename, "")
                    End If

                    'XML Transformation, XSLT
                    If xsltFile <> "" Then
                        If strTempDoc <> "" Then
                            xmlDoc = New XmlDocument
                            xmlDoc.PreserveWhitespace = True

                            xmlDoc.LoadXml(strTempDoc)
                        ElseIf xmlDoc Is Nothing Then
                            xmlDoc = New XmlDocument
                            xmlDoc.PreserveWhitespace = True
                            Dim sr As StreamReader = New StreamReader(strFilename, System.Text.Encoding.GetEncoding("ISO-8859-1"))

                            Dim content As String = sr.ReadToEnd()
                            sr.Close()
                            sr.Dispose()

                            WriteLog(_logFilePath, "Reading document content from " & strFilename)
                            xmlDoc.LoadXml(content)

                        End If
                        Try
                            If rssOutput Then
                                strTempDoc = XsltProc.DoXslTransform(xmlDoc, jobDefId, customerId, row, bXsltWithParams, True, strOutPath.Substring(strOutPath.Substring(0, strOutPath.LastIndexOf(Path.DirectorySeparatorChar)).LastIndexOf(Path.DirectorySeparatorChar) + 1).Replace(Path.DirectorySeparatorChar, "/") & "/RSSFeed" & strSuffix)
                            Else
                                strTempDoc = XsltProc.DoXslTransform(xmlDoc, jobDefId, customerId, row, bXsltWithParams, False, "")
                            End If
                        Catch e As Exception
                            WriteErr(_logFilePath, "Feil i Xslt", e)
                            strTempDoc = "<error><message>" & e.Message & "</message><stacktrace>" & e.StackTrace & "</stacktrace></error>"
                        End Try
                    End If

                    'Find and Replace after XSLT
                    If xchgAfterId > 0 Then
                        strTempDoc = DoXchg(xchgAfterId, strFilename, strTempDoc)
                    End If

                    ' Make temp file cache for all other customers who need this file
                    WriteLog(_logFilePath, "Encoding: " & encoding)
                    WriteFile(tmpFile, strTempDoc, False, encoding)

                    If Not bXsltWithParams Then
                        _htTempFiles.Item(jobDefId) = tmpFile
                    End If
                Else
                    strTempDoc = ReadFile(tmpFile)
                End If
            End If

            'Kopier fil til Kundens mappe
            Try
                Dim running As Boolean = False
                Try
                    running = SmtpRunning(_logFilePath)
                Catch
                End Try

                If bEMail And running Then
                    MailDistribution(rowCustomer, row, strSuffix, jobDefId, tmpFile, strTempDoc, xmlDoc)
                ElseIf Not bEMail Then
                    CopyToDistribution(rowCustomer, maxItemsRss, strSuffix, strFilename, tmpFile, strTempDoc, rssOutput, strOutPath, encoding, delay)
                End If
            Catch e As Exception
                WriteErr(_logFilePath, "Feil i filkopiering", e)
                Return False
            End Try

        Next
        Return True
    End Function

    Private Sub CopyToDistribution(rowCustomer As DataRow, maxItemsRss As Integer, strSuffix As String, strFilename As String, tmpFile As String, strTempDoc As String, rssOutput As Boolean, strOutPath As String, encoding As String, delay As Integer)
        Dim strOutFile As String

        'Copy file to destination
        strOutFile = strOutPath & Path.DirectorySeparatorChar & Path.GetFileNameWithoutExtension(strFilename) & strSuffix

        Dim msg As String = ""
        If delay > 0 Then
            strOutFile = strOutFile.Replace(_strDefOutPath, _strDelayPath)
            msg = " ( Delay: " & delay & " )"
        End If

        If rssOutput Then
            Dim xmlNewDoc As XmlDocument = New XmlDocument
            xmlNewDoc.PreserveWhitespace = True
            xmlNewDoc.LoadXml(strTempDoc)
            Dim datetimePublication As String = xmlNewDoc.SelectSingleNode("/rss/channel/pubDate").InnerText.Replace("T", " ").Insert(4, "-").Insert(7, "-").Insert(13, ":").Insert(16, ":")
            Dim publicationDate As DateTime = DateTime.Parse(datetimePublication)
            publicationDate = publicationDate.ToUniversalTime()
            xmlNewDoc.SelectSingleNode("/rss/channel/pubDate").InnerText = publicationDate.ToString("r")
            xmlNewDoc.SelectSingleNode("/rss/channel/item/pubDate").InnerText = publicationDate.ToString("r")

            Dim xmlOldDoc As XmlDocument = New XmlDocument
            xmlOldDoc.PreserveWhitespace = True

            Try
                strOutFile = strOutFile.Substring(0, strOutFile.LastIndexOf(Path.DirectorySeparatorChar)) & Path.DirectorySeparatorChar & "RSSFeed" & strSuffix

                Dim streamReader As StreamReader = New StreamReader(strOutFile, System.Text.Encoding.GetEncoding("ISO-8859-1"))
                Dim content As String = streamReader.ReadToEnd()

                streamReader.Close()
                streamReader.Dispose()

                xmlOldDoc.LoadXml(content)

                Dim remC As Integer
                Dim channelNode As XmlNode = xmlOldDoc.SelectSingleNode("/rss/channel")
                Dim nd As XmlNode
                Dim ndList As XmlNodeList
                Dim newNode As XmlNode = xmlOldDoc.ImportNode(xmlNewDoc.SelectSingleNode("/rss/channel").Item("item"), True)

                'Duplikatsjekk
                Try
                    nd = channelNode.SelectSingleNode("item[guid='" + newNode.Item("guid").InnerText + "']")
                    While Not nd Is Nothing
                        channelNode.RemoveChild(nd)
                        nd = channelNode.SelectSingleNode("item[guid='" + newNode.Item("guid").InnerText + "']")
                    End While

                Catch locateDuplicatesException As Exception
                    WriteErr(_logFilePath, "RSS duplikatsjekk feilet: " & rowCustomer("CustomerName") & ": " & Path.GetDirectoryName(strOutFile), locateDuplicatesException)
                End Try

                'Fjern overfl�dige
                ndList = channelNode.SelectNodes("item")
                newNode = channelNode.InsertBefore(newNode, ndList(0))

                remC = ndList.Count - maxItemsRss

                For i As Integer = 1 To remC
                    channelNode.RemoveChild(channelNode.LastChild)
                Next

                xmlOldDoc.SelectSingleNode("/rss/channel/lastBuildDate").InnerText = DateTime.Now.ToUniversalTime().ToString("r") '.Replace("-", "").Replace(":", "")
                xmlOldDoc.SelectSingleNode("/rss/channel/pubDate").InnerText = publicationDate.ToString("r")
                strTempDoc = xmlOldDoc.OuterXml()
            Catch e As Exception
                Try
                    strTempDoc = xmlNewDoc.OuterXml()
                Catch
                    WriteErr(_logFilePath, "RSS feed feilet: " & rowCustomer("CustomerName") & ": " & Path.GetDirectoryName(strOutFile), e)

                End Try
            End Try
            ' Make temp file cache for all other customers who need this file
            WriteFile(tmpFile, strTempDoc.Trim(), False, encoding)
        End If

        File.Copy(tmpFile, strOutFile, True)

        WriteLog(_logFilePath, "Kopiert til: " & rowCustomer("CustomerName") & ": " & Path.GetDirectoryName(strOutFile) & msg)
    End Sub

    Private Sub MailDistribution(rowCustomer As DataRow, row As DataRow, strSuffix As String, jobDefId As Integer, tmpFile As String, strTempDoc As String, xmlDoc As XmlDocument)
        Dim toEmail As String


        ' Sender fila som vedlegg i E-post
        If Not IsDBNull(row("SendToEmail")) Then
            toEmail = row("SendToEmail")

            Dim message As Net.Mail.MailMessage
            message = _htMailCache(jobDefId)

            If message Is Nothing Then
                message = New Net.Mail.MailMessage
                Dim secondarySenderCriteriaContent As String = ""
                Dim senderName As String = PrimarySenderName
                Dim senderAddress As String = PrimarySenderAddress
                Try
                    If SecondarySenderCriteria <> "" Then
                        secondarySenderCriteriaContent = xmlDoc.SelectSingleNode(SecondarySenderCriteria).InnerText
                    End If

                    If (secondarySenderCriteriaContent = SecondarySenderCriteriaValue1 Or secondarySenderCriteriaContent = SecondarySenderCriteriaValue2) Then
                        senderName = SecondarySenderName
                        senderAddress = SecondarySenderAddress
                    End If
                    Try
                        message.Subject = senderName & " " & _
                                            xmlDoc.SelectSingleNode("/nitf/head/tobject/@tobject.type").InnerText & ": " & _
                                            xmlDoc.SelectSingleNode("/nitf/body/body.head/hedline/hl1").InnerText
                    Catch
                        message.Subject = "Nyhet fra " & senderName
                    End Try
                Catch
                    message.Subject = "Nyhet fra " & senderName
                End Try
                If strSuffix.ToLower().StartsWith(".htm") Then
                    MakeNewMessage(message, tmpFile, "html", strTempDoc, senderName, senderAddress)
                ElseIf strSuffix.ToLower() = ".txt" Then
                    MakeNewMessage(message, tmpFile, "text", strTempDoc, senderName, senderAddress)
                Else
                    MakeNewMessage(message, tmpFile, "text", "", senderName, senderAddress)
                End If
                _htMailCache(jobDefId) = message
            End If

            If toEmail.Contains(";") Then
                Dim emailAddresses As Array = toEmail.Split(";")

                For counter As Integer = 0 To emailAddresses.Length - 1
                    If Not String.IsNullOrEmpty(emailAddresses(counter).ToString().Trim()) Then
                        message.To.Add(emailAddresses(counter))
                    End If
                Next
            Else
                message.To.Add(New MailAddress(toEmail))
            End If

            Try
                If message.To.Count > 0 Then
                    WriteLog(_logFilePath, String.Format("Sending mail to {0}", message.To.Item(0)))
                End If

                Dim smtpServerPort As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("SmtpServerPort"))

                If SmtpServer.Trim() = String.Empty Then
                    SmtpServer = ConfigurationManager.AppSettings("SmtpServer")
                End If

                If message.To.Count > 0 Then
                    Dim smtpClient As SmtpClient = New SmtpClient(SmtpServer, smtpServerPort)

                    Try
                        smtpClient.Send(message)
                    Catch ex As Exception
                        WriteErr(_logFilePath, "Error when sending e-mail to customer!", ex)
                    End Try

                    ' Disposing message so we release the file
                    message.Dispose()

                    ' disposing the smtp-client so we have stopped this as well
                    smtpClient.Dispose()
                End If

                WriteLog(_logFilePath, "Sendt p� Epost til: " & rowCustomer("CustomerName") & "/" & toEmail & " : " & Path.GetDirectoryName(tmpFile))
            Catch ex As Exception
                WriteErr(_logFilePath, "Error when sending e-mail to customer!", ex)
            End Try

        End If

    End Sub

    Private Sub MakeNewMessage(ByRef message As Net.Mail.MailMessage, ByVal tmpFile As String, ByVal format As String, Optional ByVal strHtmlDoc As String = "", Optional ByVal senderName As String = "NTB", Optional ByVal senderAddress As String = "ntb@ntb.no")

        Try
            If senderAddress.Trim() = String.Empty Then
                senderAddress = "ntb@ntb.no"
                senderName = "NTB"
            End If

            message.From = New MailAddress(senderAddress)
            ' message.BodyEncoding = MyEncoding

            message.IsBodyHtml = False

            If format.ToLower() = "html" Then
                message.IsBodyHtml = True
            Else
                message.IsBodyHtml = False
            End If

            If strHtmlDoc = "" Then
                Dim bodyMessage As String = String.Format("Nyheter fra {0}, se vedlegg {1}", senderName, vbCrLf)
                message.Body = bodyMessage
                WriteLog(_logFilePath, String.Format("Attaching {0} to mail", tmpFile))

                Dim attachment As Net.Mail.Attachment = New Net.Mail.Attachment(tmpFile, MediaTypeNames.Application.Octet)
                message.Attachments.Add(attachment)
            Else
                message.Body = strHtmlDoc
            End If
        Catch ex As Exception

            WriteErr(_logFilePath, "An error occured while trying to create message. Sender address: '" & senderAddress & "'", ex)
        End Try


    End Sub

    Public Function WaitWhileBusy(ByVal sleepRetrySeconds As Integer, Optional ByVal timeOutSeconds As Integer = 15) As Boolean
        For i As Integer = 0 To timeOutSeconds / sleepRetrySeconds
            If BBusy Then
                Sleep(sleepRetrySeconds * 1000)
            Else
                Return True
            End If
        Next
        Return False
    End Function

    Private Function DoXchg(ByVal xchgId As Integer, ByRef tmpFile As String, ByVal strTempDoc As String) As String
        If strTempDoc = "" Then
            Dim sr As StreamReader = New StreamReader(tmpFile)

            strTempDoc = sr.ReadToEnd() ' ReadFile(tmpFile)
            sr.Close()
            sr.Dispose()

            If strTempDoc = "" Then
                Return ""
            End If
        End If

        Try
            Return XchgReplace.DoXchgReplace(xchgId, strTempDoc)
        Catch e As Exception
            WriteErr(_logFilePath, "Feil i DoXchgReplace", e)
            Return strTempDoc
        End Try

    End Function

    'Checks folders for waiting files
    Private Sub WatchFolders()

        Dim row As DataRow
        For Each row In _dsWatchFolder.Tables(0).Rows

            Dim id As Integer
            id = row("CustomerId") & ""
            Dim threshold As Integer
            threshold = row("Threshold") & ""

            Dim email As String
            email = row("ContactEmail") & ""

            Dim name As String
            name = row("CustomerName") & ""

            Dim cell As String
            cell = row("ContactMobile") & ""

            Dim path As String
            path = row("OutPath") & ""

            Dim filter As String
            filter = "*." & row("Suffix")

            Dim files As String()

            Dim msg As String = String.Empty

            If filter = "*." Then filter &= "xml"

            'Check if error is sent within last hour
            Dim sendError As Boolean = True
            If _htErrorStates.ContainsKey(path) Then
                sendError = Now.Subtract(_htErrorStates(path)).Totalminutes > 60
            End If

            'Check if any old files is waiting
            If sendError Then
                sendError = False

                files = Directory.GetFiles(path, filter)
                For Each f As String In files
                    Dim dt As Date = File.GetLastWriteTime(f)

                    If Now.Subtract(dt).TotalMinutes > threshold Then
                        sendError = True
                    End If

                    msg &= f & vbCrLf
                Next

                'Send error
                If sendError Then

                    'Mail
                    msg = "Det har oppst�tt en feil i FTP-distribusjonen." & vbCrLf & vbCrLf & _
                        "Det ligger " & files.GetLength(0) & " filer i k� for overf�ring til " & name & vbCrLf & _
                        "i mappen '" & path & "'." & vbCrLf & vbCrLf & msg

                    Dim message As Net.Mail.MailMessage = New Net.Mail.MailMessage()


                    message.To.Add(New MailAddress(_errorMailAddress))
                    message.To.Add(New MailAddress(email))

                    message.From = New MailAddress("505@ntb.no")
                    message.Subject = "Feil ved FTP-overf�ring til " & name
                    message.Body = msg

                    Dim smtpClientPort As Integer = Convert.ToInt32(ConfigurationManager.AppSettings("SmtpServerPort"))
                    Dim smtpClient As Net.Mail.SmtpClient = New SmtpClient(SmtpServer, smtpClientPort)

                    Try
                        smtpClient.Send(message)
                    Catch
                        sendError = False
                    End Try

                    'SMS
                    Dim num As String
                    For Each num In _errorSmsNumber.Split(";")
                        If num <> "" And sendError Then
                            msg = "[SMS]" & vbCrLf
                            msg &= "From=NTB-505" & vbCrLf
                            msg &= "To=" & num & vbCrLf
                            msg &= "Text=NTB Distribusjon: Det ligger " & files.GetLength(0) & " filer i k� for overf�ring til " & name
                            WriteFile(SmsFolder & "\SM" & num & id & ".GSM", msg, False)
                        End If
                    Next

                    For Each num In cell.Split(";")
                        If num <> "" And sendError Then
                            msg = "[SMS]" & vbCrLf
                            msg &= "From=NTB-505" & vbCrLf
                            msg &= "To=" & num & vbCrLf
                            msg &= "Text=NTB Distribusjon: Det ligger " & files.GetLength(0) & " filer i k� for overf�ring til " & name
                            WriteFile(SmsFolder & "\SM" & num & id & ".GSM", msg, False)
                        End If
                    Next

                    _htErrorStates(path) = Now

                    WriteLogNoDate(_logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
                    WriteLog(_logFilePath, "Overv�kning - " & files.GetLength(0) & " filer i k� til " & name)
                    WriteLog(_logFilePath, "Overv�kning - " & path)
                ElseIf _htErrorStates.ContainsKey(path) Then
                    _htErrorStates.Remove(path)
                End If
            End If
        Next

    End Sub

    Private Sub MoveDelayed()

        Dim logitem As String
        logitem = ""

        Dim row As DataRow
        For Each row In _dsDelayFolder.Tables(0).Rows

            Dim name As String
            name = row("CustomerName") & ""

            Dim delay As Integer
            delay = row("PushDelay") & ""

            Dim oPath As String
            oPath = row("OutPath") & ""

            Dim dPath As String
            dPath = oPath.Replace(_strDefOutPath, _strDelayPath)

            Dim filter As String
            filter = "*." & row("Suffix")

            If filter = "*." Then
                filter &= "xml"
            End If

            Dim files As String()
            files = Directory.GetFiles(dPath, filter)

            For Each f As String In files
                Dim dt As Date = File.GetLastWriteTime(f)

                If Now.Subtract(dt).TotalMinutes > delay Then
                    'Move file back to original folder
                    File.Copy(f, oPath & "\" & Path.GetFileName(f), True)
                    File.Delete(f)
                    logitem &= Format(Now, DATE_TIME) & ": Delay ( " & name & ", " & delay & " min. ) - " & Path.GetFileName(f) & " kopiert til kundemappe." & vbCrLf
                End If
            Next
        Next

        If logitem <> "" Then
            WriteLogNoDate(_logFilePath, "---------------------------------------------------------------------------------------------------------------------------")
            WriteLogNoDate(_logFilePath, logitem.TrimEnd())
        End If
    End Sub

End Class
