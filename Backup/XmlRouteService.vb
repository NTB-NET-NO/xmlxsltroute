Imports System.IO
Imports System.Xml
Imports System.Xml.Xsl
Imports System.Data.OleDb
Imports System.ServiceProcess

#If Debug Then
Public Module XmlRouteService
#Else
Public Class XmlRouteService : Inherits ServiceBase
#End If

    'Dim Debug
    '-- Lokale variabler --

    Private htDistKodeSatAviser As Hashtable
    Private strReInitFile As String

    Private xsltPath As String
    Private logFilePath As String

    Private objXchgReplace As XchgReplace
	Private collXmlRoute As Collection	'= New Collection()

    Private dsSystem As DataSet
    Private dsGroupList As DataSet
    Private dsDistKodeSatAviser As DataSet
    Private dsXPath As DataSet

    'Win API function Sleep
    Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)

#If Debug Then
    ' For debug: Run as form
	Public Sub Main()

		InitXmlRoute()
		Start()
		StopWait()
		Dim strTest As String
		Do
			Console.WriteLine("MENY: ")
			Console.WriteLine("P = Pause")
			Console.WriteLine("S = Start")
			Console.WriteLine("R = Reinit")
			Console.WriteLine("D = Dispose")
			Console.WriteLine("Q = Quit: ")
			Console.Write("Trykk menyvalg: ")
			strTest = Console.ReadLine
			Select Case UCase(strTest)
				Case "Q"
					Exit Do
				Case "P"
					StopWait()
				Case "S"
					Start()
				Case "R"
					'Reinit()
					DisposeObjects()
					InitXmlRoute()
					Start()
				Case "D"
					DisposeObjects()
			End Select
		Loop
		StopWait()

	End Sub
#Else

    ' For Release: run as service
    Public Shared Sub Main()
        ServiceBase.Run(New XmlRouteService())
    End Sub

    Public Sub New()
        MyBase.New()
        CanPauseAndContinue = False
        CanStop = True
        ServiceName = "NTB_XmlXsltRoute"
    End Sub

    Protected Overrides Sub OnStop()
		DisposeObjects()
		EventLog.WriteEntry(TEXT_STOPPED)
        'End
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
		Try
			if InitXmlRoute() then Start()
			EventLog.WriteEntry(TEXT_STARTED)
		Catch e As Exception
            WriteErr(errFilePath, "Feil i initiering av programmet", e)
            End
        End Try
    End Sub
#End If

    Private Function InitXmlRoute() As Boolean
        Dim intCount As Integer
        Dim dtStartTime As Date
        Dim rowGroupList As DataRow
        Dim objXmlRoute As XmlRoute

        dtStartTime = Now

        Try
            Directory.CreateDirectory(errFilePath)
            Directory.CreateDirectory(tempPath)
            Directory.CreateDirectory(databaseOfflinePath)

            OpenDatabase()
            FillDataSets()

            If Not VerifyXPaths() Then Return False

            xsltPath = dsSystem.Tables(0).Rows(0)("xsltPath") & ""
            logFilePath = dsSystem.Tables(0).Rows(0)("logFilePath") & ""

            SMSFolder = dsSystem.Tables(0).Rows(0)("SMSFolder") & ""
            smtpServer = dsSystem.Tables(0).Rows(0)("SMTPServer") & ""

            Directory.CreateDirectory(SMSFolder)
            Directory.CreateDirectory(logFilePath)

            FillHashtables()

            'objXchgReplace = New XchgReplace()
            XchgReplace.Init(cn)
            XsltProc.Init(cn, xsltPath)

            collXmlRoute = New Collection

            For Each rowGroupList In dsGroupList.Tables(0).Rows

                objXmlRoute = New XmlRoute(cn, rowGroupList, logFilePath, databaseOfflinePath) ', xsltPath)
                objXmlRoute.htDistKodeSatAviser = htDistKodeSatAviser
                'objXmlRoute.objXchgReplace = objXchgReplace

                collXmlRoute.Add(objXmlRoute)
            Next
            CloseDatabase()
            WriteLog(logFilePath, TEXT_INIT)

        Catch e As Exception
            WriteErr(errFilePath, "Feil i initiering av XmlRoute", e)
#If DEBUG Then
            MsgBox("Feil i initiering av XmlRoute: " & e.Message & e.StackTrace)
#End If
            Return False
        End Try
        Return True
    End Function

    Private Sub StopWait()
        Dim objXmlRoute As XmlRoute
        For Each objXmlRoute In collXmlRoute
            objXmlRoute.Pause()
        Next

        For Each objXmlRoute In collXmlRoute
            objXmlRoute.WaitWhileBusy(2)
        Next
        WriteLog(logFilePath, TEXT_STOPPED)
    End Sub

    Private Sub Start()
        Dim objXmlRoute As XmlRoute
        For Each objXmlRoute In collXmlRoute
            objXmlRoute.Start()
        Next
        WriteLog(logFilePath, TEXT_STARTED)
    End Sub

    Private Function OpenDatabase() As Boolean
        Try
            cn.Open()
        Catch e As Exception
            WriteErr(errFilePath, "Feil i OpenDatabase, forsøker cachede tabeller.", e)
            Return (False)
        End Try
        Return True
    End Function

    Private Sub FillDataSets()
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter
        Dim command As OleDbCommand = New OleDbCommand

        command.Connection = cn
        dataAdapter.SelectCommand = command

        dsSystem = New DataSet
        dsGroupList = New DataSet
        dsDistKodeSatAviser = New DataSet
        dsXPath = New DataSet

        FillOneDataSet(dsSystem, "dsSystem", "SELECT * FROM System", dataAdapter, command)
        FillOneDataSet(dsGroupList, "dsGroupList", "SELECT * FROM GroupList", dataAdapter, command)
        FillOneDataSet(dsDistKodeSatAviser, "dsDistKodeSatAviser", "SELECT * FROM DistKodeSatAviser", dataAdapter, command)
        FillOneDataSet(dsXPath, "dsXPath", "SELECT * FROM GroupXPath", dataAdapter, command)

        dataAdapter = Nothing
        command = Nothing
    End Sub

    Private Sub CloseDatabase()
        Try
            cn.Close()
        Catch e As Exception
            WriteErr(errFilePath, "Feil i CloseDatabase: cn.Close", e)
        End Try
    End Sub

    Private Sub FillHashtables()
        Dim row As DataRow
        Dim strDistKode As String
        Dim jobDefID As Integer
        Dim i As Integer
        htDistKodeSatAviser = New Hashtable

        ' Legg DistKodeSatAviser i Hashtable
        For Each row In dsDistKodeSatAviser.Tables(0).Rows
            strDistKode = UCase(row("DistKodeSatAviser"))
            htDistKodeSatAviser.Item(strDistKode) = strDistKode
        Next
    End Sub


    Private Sub DisposeObjects()
        Try
            Dim objXmlRoute As XmlRoute

            StopWait()

            For Each objXmlRoute In collXmlRoute
                objXmlRoute = Nothing
            Next

            collXmlRoute = Nothing
            collXmlRoute = New Collection
            objXchgReplace = Nothing

            htDistKodeSatAviser = Nothing

            dsGroupList = Nothing
            dsDistKodeSatAviser = Nothing


        Catch e As Exception
            WriteErr(errFilePath, "Feil i DisposeObjects", e)
#If DEBUG Then
            MsgBox("Feil i initiering av XmlRoute: " & e.Message & e.StackTrace)
#End If
            End
        End Try
    End Sub

    Function VerifyXPaths() As Boolean

        Dim email As String = dsGroupList.Tables(0).Rows(0)("ErrorEmail")
        Dim content As New String("")

        Dim doc As New XmlDocument
        Dim row As DataRow

        For Each row In dsXPath.Tables(0).Rows
            Try
                Dim nodes As XmlNodeList = doc.SelectNodes(row("XPath"))
            Catch ex As Exception
                WriteErr(errFilePath, "Feil i XPath-utvalgsspørring" & row("XPathName"), ex)
                content &= "Feil i XPath-utvalgsspørringen " & row("XPathName") & vbCrLf & row("XPath") & vbCrLf & vbCrLf & ex.Message & vbCrLf & ex.StackTrace & vbCrLf & vbCrLf & vbCrLf
            End Try
        Next

        Dim running As Boolean = False
        Try
            running = SMTPRunning()
        Catch
        End Try

        If content <> "" Then
            If running Then
                System.Web.Mail.SmtpMail.SmtpServer = smtpServer
                System.Web.Mail.SmtpMail.Send(email, email, "Feil i XPath utvalgsspørring!", content)
            End If
            Return False
        Else
            Return True
        End If

    End Function


#If DEBUG Then
End Module
#Else
End Class
#End If
