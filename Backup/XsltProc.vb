Imports System.Xml
Imports System.Xml.Xsl
Imports System.Data.OleDb
Imports msxml2


Public Class XsltProc

    Private Shared dsCustomerJob As DataSet
    Private Shared dsXsltJobs As DataSet
    Private Shared htXsltProcs As Hashtable
    Private Shared htArgumentLists As Hashtable = New Hashtable()

    Public Shared Sub Init(ByRef cn As OleDbConnection, ByVal strXsltPath As String)

        ' Fill Datasets
        Dim dataAdapter As OleDbDataAdapter = New OleDbDataAdapter()
        Dim command As OleDbCommand = New OleDbCommand()

        dataAdapter.SelectCommand = command
        command.Connection = cn

        dsCustomerJob = New DataSet()
        dsXsltJobs = New DataSet()

        'FillOneDataSet(dsCustomerJob, "dsCustomerJobParam", "SELECT * FROM GetCustomerJob WHERE GroupListID", dataAdapter, command)
        FillOneDataSet(dsCustomerJob, "dsCustomerJobParam", "SELECT * FROM GetCustomerJob", dataAdapter, command)
        FillOneDataSet(dsXsltJobs, "dsXsltJobs", "SELECT * FROM GetXsltJobs", dataAdapter, command)

        dataAdapter = Nothing
        command = Nothing

        ' Fill hashtables
        Dim xsltFile As String
        htXsltProcs = New Hashtable()

        'Lag XSLT prosessors for hver XSLT-fil og legg i Hashtable
        Dim jobDefID As Integer
        Dim row As DataRow
        For Each row In dsXsltJobs.Tables(0).Rows
            jobDefID = row("JobDefID")
            xsltFile = row("XsltFile") & ""
            If xsltFile <> "" Then

                'Dim xsltProc As Xsl.XslTransform = New Xsl.XslTransform()

                ''Try
                'Dim doc As XmlDocument = New XmlDocument()
                'doc.Load(strXsltPath & "\" & xsltFile)

                'xsltProc.Load(doc)

                'Bugfix: rewritten to use MSXML4
                'Dim xsltDoc As MSXML2.FreeThreadedDOMDocument
                'xsltDoc = New MSXML2.FreeThreadedDOMDocument()

                'xsltDoc.load(strXsltPath & "\" & xsltFile)

                'xslt.stylesheet = xsltDoc
                'xsltProc = xslt.createProcessor

                htXsltProcs.Add(jobDefID, strXsltPath & "\" & xsltFile)

                'Catch e As Xsl.XsltException
                '    WriteErr(errFilePath, "Feil i lasting av XSLT file: " & xsltFile, e)
                'End Try
            End If
        Next

        'Lag XsltArgumentList for hver Kunde og legg i Hashtable
        Dim xslParam As Xsl.XsltArgumentList = New Xsl.XsltArgumentList()
        For Each row In dsCustomerJob.Tables(0).Rows
            Dim strParam As String = row("XsltParamsCust") & ""
            If strParam <> "" Then
                Dim customerID As Integer = row("customerID")
                jobDefID = row("jobDefID")

                Dim arrParams As String() = Split(strParam, vbCrLf)
                For Each strParam In arrParams
                    Dim strParamValue As String = row(strParam) & ""
                    xslParam.AddParam(strParam, "", strParamValue)
                Next
                htArgumentLists.Add(customerID & "-" & jobDefID, xslParam)
            End If
        Next

    End Sub

    Public Shared Function DoXslTransform(ByVal xmlDoc As Xml.XmlDocument, ByVal jobDefId As Integer, ByVal customerId As Integer, ByVal row As DataRow, ByRef bXsltWithParams As Boolean, ByVal bLastBuildDateParam As Boolean, ByVal strRelativePath As String) As String
        Dim strErr As String

        Dim xsltDoc As MSXML2.FreeThreadedDOMDocument = New MSXML2.FreeThreadedDOMDocument
        Dim xslt As MSXML2.IXSLTemplate = New MSXML2.XSLTemplate

        Dim xsltProc As MSXML2.IXSLProcessor
        Dim xsltParam As Xsl.XsltArgumentList

        ' Getting XsltArgumentList object (loaded from dataset at init) from hashtable
        xsltParam = htArgumentLists(customerId & "-" & jobDefId)

        If xsltParam Is Nothing Then
            xsltParam = New XsltArgumentList
        End If

        If bLastBuildDateParam Then
            xsltParam.AddParam("lastBuildDate", "", DateTime.Now.ToUniversalTime().ToString("r")) '.Replace("-", "").Replace(":", ""))
        End If
        If strRelativePath <> "" Then
            xsltParam.AddParam("relativeOutputPath", "", strRelativePath)
        End If

        If Not xsltParam Is Nothing Then
            bXsltWithParams = True
        End If

        ' Getting XslTransform object (loaded with xslt-file at init) from hashtable

        'If xslProc Is Nothing Then Return "Empty XslTransform object!"

        Try
            If htXsltProcs.Contains(jobDefId) Then
                xsltDoc.load(htXsltProcs.Item(jobDefId))

                xslt.stylesheet = xsltDoc
                xsltProc = xslt.createProcessor
            End If
            If bLastBuildDateParam Then
                xsltProc.addParameter("lastBuildDate", DateTime.Now.ToUniversalTime().ToString("r")) '.Replace("-", "").Replace(":", ""))
            End If
            If strRelativePath <> "" Then
                xsltProc.addParameter("relativeOutputPath", strRelativePath)
            End If
            'Dim swOutput As IO.Stream = New IO.MemoryStream()

            'xsltProc.Transform(xmlDoc, xsltParam, swOutput)

            ''swOutput.Flush()
            ''End If
            'Dim strTemp As String
            'Dim reader As IO.StreamReader = New IO.StreamReader(swOutput, myEncoding)

            'swOutput.Position = 0
            'strTemp = reader.ReadToEnd
            'reader.Close()

            'Bugfix: rewritten to use MSXML4
            Dim xml4Doc As MSXML2.DOMDocument = New MSXML2.DOMDocument
            Dim strTemp As String

            
            xml4Doc.loadXML(xmlDoc.OuterXml)
            xsltProc.input = xml4Doc
            If (Right(htXsltProcs.Item(jobDefId), Len("nitf2rss_full.xsl")) = "nitf2rss_full.xsl") Then
                Dim xsltHelper As New XsltHelperFunctions
                xsltProc.addObject(xsltHelper, "urn:XsltHelperFunctions")
            End If
            xsltProc.transform()
            strTemp = xsltProc.output

            Return strTemp

        Catch e As Exception
            'strErr = "Error: Line number: " & e.LineNumber & ", Position: " & e.LinePosition & vbCrLf & e.Message & e.StackTrace
            WriteErr(errFilePath, "Feil i XsltTransform", e)
#If DEBUG Then
            MsgBox(e.Message)
#End If
            Return "<error>" & e.Message & "</error>"
        End Try

    End Function



End Class
