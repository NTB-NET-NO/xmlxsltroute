<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="iso-8859-1" indent="yes" standalone="yes"/>

<!-- 
	Styleshet for transformasjon fra NTB internt NITF-format til NITF for SMS-kunder
	Sist endret Av Roar Vestre 31.10.2002
-->

<xsl:template match="/nitf">
<nitf version="-//IPTC-NAA//DTD NITF-XML 2.5//EN" change.date="9 august 2000" change.time="1900" baselang="no-NO">
<head>
<xsl:copy-of select="head/title | head/docdata | head/pubdata | head/revision-history  | head/tobject"/>
</head>
<body>
	<xsl:copy-of select="//body.head"/>
	<xsl:copy-of select="//body.content"/>
	<sms><xsl:value-of select="head/meta[@name='ntb-sms']/@content"/></sms>
	<xsl:copy-of select="//body.end"/>
</body>		
</nitf>
</xsl:template>

</xsl:stylesheet>