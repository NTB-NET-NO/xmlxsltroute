<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" version="1.0" encoding="iso-8859-1" omit-xml-declaration="yes"
	doctype-system="nornitf.dtd" indent="yes" standalone="yes"/>
	
	<!-- Create som params for easy access later -->	
	<xsl:param name="net-size">
		<xsl:value-of select="string-length(nitf/body/body.content)"/>
	</xsl:param>
	<xsl:param name="gross-size">
		<xsl:value-of select="string-length(nitf/body)"/>
	</xsl:param>
	
	<!-- Identity string -->
	<xsl:param name="ident">
		<xsl:value-of select="substring-before(nitf/head/pubdata/@date.publication,'T')"/>
		<xsl:text>:NTB:</xsl:text>
		<xsl:value-of select="nitf/head/meta[@name='NTBIPTCSequence']/@content"/>
		<xsl:text>:0</xsl:text>
	</xsl:param>

	<!-- Stoffgruppe mapping, defaults to DOM (Innenriks) -->
	<xsl:param name="TKOD">
		<xsl:choose>
			<xsl:when test="nitf/head/tobject/@tobject.type = 'Sport'">
				<xsl:text>SPT</xsl:text>
			</xsl:when>
			<xsl:when test="nitf/head/tobject/@tobject.type = 'Innenriks'">
				<xsl:text>DOM</xsl:text>
			</xsl:when>
			<xsl:when test="nitf/head/tobject/@tobject.type = 'Utenriks'">
				<xsl:text>FOR</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>DOM</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:param>

	<!--Main template, NITF root-->
	<xsl:template match="nitf">
		<NORNITF>
		<HEAD>
			<xsl:apply-templates select="head"/>
		</HEAD>

		<BODY ID="8:10">
			<xsl:apply-templates select="body"/>
		</BODY>
		
		<ENDSIZE ID="9:10">
			<xsl:value-of select="$net-size"/>
		</ENDSIZE>
		</NORNITF>
	</xsl:template>

	<!-- Head template -->
	<xsl:template match="head">
		<MODVER ID="1:00" VER="04"/>

		<!-- Destinations -->
		<xsl:if test="contains(meta[@name='NTBSendTilNorden']/@content,'FNB')">
			<DEST ID="1:05">FNB</DEST>
		</xsl:if>
		<xsl:if test="contains(meta[@name='NTBSendTilNorden']/@content,'RB')">
			<DEST ID="1:05">RBN</DEST>
		</xsl:if>
		<xsl:if test="contains(meta[@name='NTBSendTilNorden']/@content,'TT')">
			<DEST ID="1:05">TTN</DEST>
		</xsl:if>

		<FIFO ID="1:22" FFNO="99"/>
		<FIFOVER ID="1:22" FFVERNO="01"/>
		<SERVID ID="1:30">NTB</SERVID>
		<NR ID="1:40">
			<xsl:value-of select="meta[@name='NTBIPTCSequence']/@content"/>
		</NR>

		<PRODID ID="1:50" TKOD="{$TKOD}"/>
		<DATESENT ID="1:70">
			<xsl:value-of select="substring-before(pubdata/@date.publication,'T')"/>
		</DATESENT>
		<TIMESENT ID="1:80">
			<xsl:value-of select="substring-after(pubdata/@date.publication,'T')"/>
			<xsl:text>+0100</xsl:text>
		</TIMESENT>
		<UNO ID="1:100">
			<xsl:value-of select="$ident"/>
		</UNO>

		<RECVER ID="2:00" VERNO="04"/>

		<SLUGG ID="2:05">
			<xsl:value-of select="meta[@name='subject']/@content"/>
		</SLUGG>

		<URG ID="2:10" PRIO="{docdata/urgency/@ed-urg}"/>

		<xsl:apply-templates select="tobject/tobject.subject[not(@tobject.subject.matter)]" />

		<WRITER ID="2:122">
			<xsl:value-of select="revision-history/@name"/>
		</WRITER>

		<LANG ID="2:135">nor</LANG>
		<SIZEMOD ID="7:10" SIZMOD="1"/>
		<MAXSUBSIZE ID="7:20">
			<xsl:value-of select="$gross-size"/>
		</MAXSUBSIZE>
		<SIZEANNO ID="7:90">
			<xsl:value-of select="$net-size"/>
		</SIZEANNO>
	</xsl:template>

	<!-- Body template -->
	<xsl:template match="body">
		<ALLTEXT>
		<HEADLINE>
			<xsl:value-of select="body.head/hedline/hl1"/>
		</HEADLINE>
	
		<LEAD>
			<xsl:apply-templates select="../head/docdata/ed-msg"/>
			<xsl:apply-templates select="body.content/p[@class = 'lead']"/>
		</LEAD>
		<TEXT>
			<xsl:apply-templates select="body.content/p[@class != 'lead']"/>
		</TEXT>
		</ALLTEXT>
	</xsl:template>

	<xsl:template match="*">
	</xsl:template>
	
	<xsl:template match="tobject.subject">
		<SUBREF ID="2:12">
			<xsl:text>IPTC:</xsl:text>
			<xsl:value-of select="@tobject.subject.refnum"/>
			<xsl:text>:</xsl:text>
			
			<xsl:choose>
				<xsl:when test="@tobject.subject.code = 'OKO'">EBF</xsl:when>
				<xsl:when test="@tobject.subject.code = 'KRE'">CLJ</xsl:when>
				<xsl:when test="@tobject.subject.code = 'KUL'">ACE</xsl:when>
				<xsl:when test="@tobject.subject.code = 'ULY'">DIA</xsl:when>
				<xsl:when test="@tobject.subject.code = 'UTD'">EDU</xsl:when>
				<xsl:when test="@tobject.subject.code = 'NAT'">ENI</xsl:when>
				<xsl:when test="@tobject.subject.code = 'MED'">HEA</xsl:when>
				<xsl:when test="@tobject.subject.code = 'ARB'">LBR</xsl:when>
				<xsl:when test="@tobject.subject.code = 'FRI'">LFL</xsl:when>
				<xsl:when test="@tobject.subject.code = 'REL'">RLB</xsl:when>
				<xsl:when test="@tobject.subject.code = 'VIT'">SCT</xsl:when>
				<xsl:when test="@tobject.subject.code = 'SOS'">SOI</xsl:when>
				<xsl:when test="@tobject.subject.code = 'KRI'">UCW</xsl:when>
				<xsl:when test="@tobject.subject.code = 'VAR'">WEA</xsl:when>
				<xsl:otherwise><xsl:value-of select="@tobject.subject.code"/></xsl:otherwise>
			</xsl:choose>
		</SUBREF>
	</xsl:template>

	<xsl:template match="ed-msg[string-length(@info) > 0]">
		<P>
		<xsl:value-of select="@info"/>
		</P>
	</xsl:template>

	<xsl:template match="p[string-length(normalize-space(.)) > 0]">
		<P>
		<xsl:value-of select="."/>
		</P>
	</xsl:template>

</xsl:stylesheet>