<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="text"/>

<xsl:variable name="CRLF">
	<xsl:text>&#13;&#10;&#13;&#10;</xsl:text>
</xsl:variable>

<xsl:variable name="pubdate">
	<xsl:value-of select="/nitf/head/meta[@name='ntb-dato']/@content"/>
</xsl:variable>

<xsl:template match="/nitf">
	<xsl:apply-templates select="body"/>
</xsl:template>

<xsl:template match="body">
	<xsl:value-of select="body.head/hedline/hl1"/>
	<xsl:value-of select="$CRLF"/>

	<xsl:text>Publisert: </xsl:text>
	<xsl:value-of select="$pubdate"/>
	<xsl:value-of select="$CRLF"/>

	<xsl:value-of select="body.head/byline"/>
	<xsl:value-of select="$CRLF"/>

	<xsl:apply-templates select="body.content/p | body.content/hl2 | body.content/table"/>

	<xsl:value-of select="normalize-space(body.end/tagline)"/>
</xsl:template>

<xsl:template match="p[string-length(.) &gt; 1]">
	<xsl:value-of select="."/>
	<xsl:value-of select="$CRLF"/>
</xsl:template>

<xsl:template match="p[@class='table-code']">
<!-- Paragraph of "tabellkode"
@txt:<xsl:value-of select="."/>
 -->
</xsl:template>

<xsl:template match="hl2">
	<xsl:value-of select="."/>
	<xsl:value-of select="$CRLF"/>
</xsl:template>

<xsl:template match="table">
<!-- Tabeller -->
<xsl:text>
</xsl:text>

<xsl:apply-templates select="tr"/>

<xsl:text>
</xsl:text>

</xsl:template>

<xsl:template match="tr">
<xsl:apply-templates select="td"/>
<xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="td">
<xsl:value-of select="."/>
<xsl:text>	</xsl:text>
</xsl:template>

</xsl:stylesheet>