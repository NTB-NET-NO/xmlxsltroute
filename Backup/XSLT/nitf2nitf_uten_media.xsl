<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="iso-8859-1" omit-xml-declaration="no" standalone="yes" indent="yes"/>

<!-- 
	XSLT stylesheet for transformasjon fra NTBs NITF XML-format
	til NITF uten media elementer (Bilder lyd etc.)
	Sist endret Av Roar Vestre 28.06.2005
-->

<xsl:template match="/nitf">
	<xsl:comment>&lt;!DOCTYPE nitf SYSTEM "nitf-3-2.dtd"></xsl:comment>
	<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">

		<xsl:copy-of select="head"/>

		<body>
			<xsl:apply-templates select="body/*" />
		</body>

	</nitf>
</xsl:template>

<xsl:template match="body.content">
	<body.content>
	    <xsl:apply-templates />
	</body.content>
</xsl:template>

<xsl:template match="media">
	<!-- 
	<media>Droppet media!</media> 
	-->
</xsl:template>

<xsl:template match="*">
	<xsl:copy-of select="."/>
</xsl:template>


</xsl:stylesheet>