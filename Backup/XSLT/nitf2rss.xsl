<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:param name="lastBuildDate"></xsl:param>

<xsl:param name="relativeOutputPath"></xsl:param>

<xsl:template match="/nitf">
	<rss version="2.0">
		<channel>
			<title><xsl:text>NTB RSS Feed</xsl:text></title>
			<link>
				<xsl:text>http://www.ntb.no/RSS/</xsl:text>
				<xsl:value-of select="$relativeOutputPath"/>
			</link>
			<description>NTB RSS Feed</description>
			<pubDate>
				<xsl:value-of select="head/pubdata/@date.publication"/>
			</pubDate>
			<lastBuildDate>
				<xsl:value-of select="$lastBuildDate"/>
			</lastBuildDate>
			<copyright>
				<xsl:value-of select="head/docdata/doc.copyright/@holder"/>
			</copyright>
			<docs>http://www.rssboard.org/rss-specification</docs>
		<item>
			<title>
				<xsl:value-of select="head/title | head/docdata | head/pubdata | head/revision-history  | head/tobject"/>
			</title>
			<description>
				<xsl:value-of select="body/body.content/p[@class='lead' or @lede='true']"/>
			</description>
			<pubDate>
				<xsl:value-of select="head/pubdata/@date.publication"/>
			</pubDate>
			<guid>
				<xsl:value-of select="head/docdata/du-key/@key"/>
			</guid>
		</item>
		</channel>
	</rss>
</xsl:template>

</xsl:stylesheet>