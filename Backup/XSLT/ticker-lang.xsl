<?xml version="1.0" encoding="iso-8859-1" standalone="yes" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" encoding="iso-8859-1" indent="yes"/>

<xsl:template match="nitf">
<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">

	<xsl:copy-of select="head"/>

	<body>
	<xsl:copy-of select="body/body.head" />
		<body.content>
		<xsl:apply-templates select="body/body.content/*" />
		</body.content>
	<xsl:copy-of select="body/body.end"/>
	</body>
	
</nitf>

</xsl:template>

<xsl:template match="p">
		<xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="media">
		<!-- 
		<xsl:copy-of select="."/> 
		-->
</xsl:template>

<xsl:template match="table">
		<xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="hl2">
		<xsl:copy-of select="."/>
</xsl:template>

<!-- Template som fikser opp kred-linje -->
<xsl:template match="p[@class = 'lead']">
	
	<!-- Oppretter P p� nytt -->
	<p>
	
	<!-- Kopierer class properties -->
	<xsl:attribute name="lede">
	<xsl:value-of select="@lede" />
	</xsl:attribute>

	<xsl:attribute name="class">
	<xsl:value-of select="@class" />
	</xsl:attribute>

	<!-- Legger inn innhold -->
	<xsl:value-of select="substring-before(.,'(')"/>
	<xsl:text>(NTB)</xsl:text>
	<xsl:value-of select="substring-after(.,')')"/>
	</p>

</xsl:template>
<!-- Ferdig -->

<!-- Template: Hvordan fjerne (�NTB) ved hjelp av XSLT -->
<!-- Lars Christian Hegde, NTB - 25.06.2004 -->

<!-- Lager en template som bare matcher P som inneholder (�NTB) -->
<xsl:template match="p[contains(.,'(�NTB)') ]">
	
	<!-- Oppretter P p� nytt -->
	<p>
	<!-- Kopierer class property-->
	<xsl:attribute name="class">
	<xsl:value-of select="@class" />
	</xsl:attribute>
			
	<!-- Legger inn innhold _UTEN_ (�NTB) -->
	<xsl:value-of select="substring-before(.,' (�NTB)')"/>
	</p>

</xsl:template>
<!-- Ferdig -->


</xsl:stylesheet>